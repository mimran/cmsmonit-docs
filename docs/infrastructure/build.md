# CMS Monitoring documentation 

Operators are expected to mantain and keep the docs up to date!

* The documentation is based on CERN [mkDocs](https://how-to.docs.cern.ch/new/). 
* The source code is in [gitlab](https://gitlab.cern.ch/cmsmonitoring/cmsmonit-docs/) and is deployed on an Openshift [website](https://cmsmonit-docs.web.cern.ch/)
* The structure of the website is defined by [mkdocs.yaml](https://gitlab.cern.ch/cmsmonitoring/cmsmonit-docs/-/blob/master/mkdocs.yml)
* The Openshift console is [here](https://openshift.cern.ch/console/project/cmsmonit-docs/overview)

## Markdown

Note that [mkdocs](https://www.mkdocs.org) uses [Python markdown](https://python-markdown.github.io/) to render markdown in HTML. This is slightly different from markdown in github/gitlab (especially nested lists who might look good on gitlab are not rendered correctly in html). See [here](https://www.mkdocs.org/user-guide/writing-your-docs/#writing-with-markdown).

**Check how html is rendered!** 

## How to build the website

* The build is automatically triggered by gitlab commits. Please refer to this [guide](https://how-to.docs.cern.ch/new/redeploy/) for further details. 
* The status of the build can be found in [OpenShift](https://openshift.cern.ch/console/project/cmsmonit-docs/browse/builds/docs?tab=history) build area.
