## Monitoring models

There are two conceptually different models we use in monitoring.
The *pull* model allows to scrape metrics from a given service, while
the *push* model pushes metrics to a given service. The CERN MONIT infrastructure
purely relies on the push model, while in CMS Monitoring we use
both approaches.

## The pull model

The pull model is a model used by [Prometheus](https://prometheus.io/).
The Prometheus service queries various exporters which provide a set of metrics to scrape.

## The push model

For the push model there are different solutions. Within [Prometheus](../Prometheus/README.md) we can use
the offical [pushgateway](https://github.com/prometheus/pushgateway) solution which can push metrics to Prometheus services. Please refer to the official [guide](https://prometheus.io/docs/practices/pushing/).

For [VictoriaMetrics](vm.md)
we can use [vmagent](https://victoriametrics.github.io/vmagent.html) tool
which can be installed elsewhere and push metrics to VM end-point.
