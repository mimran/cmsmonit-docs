# The CMS Monitoring infrastructure

## Architecture

The CMS Monitoring infrastructure provides several  [services](services.md) deployed on [k8s clusters](k8s.md): 

 - [Prometheus](#cms-prometheus-services) and [VictoriaMetrics](vm.md),
 - [Alerts](../AlertManagement/README.md): the [AlertManager](alertmanager.md) and the [intelligent module](../intelligence/README.md),
 - [NATS](../NATS/nats.md) for real time monitoring.

The CMS MONIT [infrastructure](../MONIT/README.md) provides [ElasticSearch](https://www.tutorialspoint.com/elasticsearch/index.htm), [InfluxDB](https://www.influxdata.com/products/influxdb-overview/), [HDFS](https://www.geeksforgeeks.org/hdfs-commands/).
You can view how all pieces are interconnected in the architectural [diagram](#architectural-diagram).

A good overview of the available services can be found in this [talk](https://indico.cern.ch/event/873410/contributions/3682300/attachments/1966507/3270012/CMS_monitoring_infrastructure.pdf). 

To interact with our services we provide [CLI tools](#cms-monitoring-cli-tools).

### Monitoring [models](models.md)

### Architectural diagram

![cluster architecture](CMSMonitoringHA.png)

### k8s clusters

The CMS Monitoring infrastructure consists of several k8s clusers (see
architecture diagram above):

- CMS [NATS](../NATS/nats.md) cluster which handles NATS services
- CMS monitoring clusters which handles Prometheus, AM, VictoriaMetrics,
  promxy, and other   services. We have the following breakdown:

  - main CMS cluster which keeps Prometheus/VM/AM and promxy services
  - two HA clusters which keep individual Prometheus/VM services
  - aggregation cluster which keeps two VM instances with different retention
    policy

    - one VM instance keeps 1h aggregated records
    - another VM instance keeps only 1d, 7d and 30d aggregated records

- spider k8s cluster to keep Spider services

### CMS Prometheus services

We use [Prometheus](../Prometheus/README.md) to monitor CMS nodes and services. It provides [PromQL](https://prometheus.io/docs/prometheus/latest/querying/basics/) (Prometheus Query Language) to query your data which is accessible from [cms-monitoring.cern.ch](https://cms-monitoring.cern.ch). 

[VictoriaMetrics](vm.md) (VM) is the back-end for Prometheus. It provides a [MetricsQL](https://victoriametrics.github.io/MetricsQL.html) which extends capability of PromQL even further.

#### Access to Prometheus and VictoriaMetrics via dashboards

Data from Prometheus/VictoriaMetrics can be visualised in [Grafana](https://github.com/dmwm/CMSMonitoring/doc/MONIT/Grafana.md) dashboards using the appropriate data sources, or indirectly via [promxy](https://github.com/jacksontj/promxy) proxy service. In promxy data-source you can use either PromQL or MetricsQL in Grafana dashboards.
We gradually migrate our infratructure to only rely on
[promxy](https://github.com/jacksontj/promxy) proxy service for access to
dashboards maintaned by Prometheus or VictoriaMetrics services.

[Instructions](https://gist.github.com/vkuznet/4ba2d063cd68fb2b912e4514b05a7081) to setup terminal based dashboard to monitor remote node with Prometheus.


## CMS Monitoring CLI tools

All CMS Monitoring tools are accessible from `/cvmfs/cms.cern.ch/cmsmon` area and in 
our [github](https://github.com/dmwm/CMSMonitoring) repository. 
The CMS monitoring tools are written in python and in [Go](https://indico.cern.ch/event/912571/contributions/3837964/note/).

They include:

### CMS Monitoring tools
- `monit` allows access to CERN MONIT data-sources like ElasticSearch and InfluxDB.
- `alert` provides access to CMS Monitoring alerts.
- `annotationManager` allows to manage annotations in dashboards
- `ggus_parser` allows to parse GGUS tickets
- `ssb_parser` allows to parse SSB tickets
- `nats-pub` and `nats-sub` are tools to connect to NATS server
- `token-manager` to handle renewal of CERN SSO tokens used in our web sites
- `promtool` to access Prometheus service
- `amtool` to access AlertManager service
- `prometheus` is Prometheus server

All CMS Monitoring tools provides comprehensive help section, just use `-help`
to see all available options for a given tool.

### kubernetes tools
These set of tools is useful to manage various tasks on kubernetes infrastrucutre
- `hey` to probe HTTP services via scalable, concurrent HTTP requests
- `k8s_info` allows to view installed images in k8s cluster
- `stern` is a tool to view kubernetes logs


