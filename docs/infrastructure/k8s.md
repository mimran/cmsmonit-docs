# The CMS monitoring k8s infrastructure

We run different k8s clusters deployed on the CERN OpenStack Kubernetes infrastructure:

- **CMS monitoring cluster** is used to run the following services:

     - cms monitoring authentication proxy server
     - [sqoop HDFS jobs](../MONIT/sources.md#additional-data-sources-from-sqoop-jobs)
     - [Rumble](../Rumble/README.md)
     - Log-Clustering HDFS service
     - NATS subscribers

- **High-Availability clusters** (referred to as HA1 and HA2) run the following services:
     - [http exporters](../Exporters/README.md)
     - [Prometheus](../Prometheus/README.md) 
     - [VictoriaMetrics](vm.md) 
     - [AlertManager](alertmanager.md)
     - [Karma dashboard](../AlertManagement/README.md#karma-dashboard) for AlertManager
     - CMS [intelligence module](../intelligence/README.md)

- **NATS cluster** is used to run [NATS](../NATS/nats.md) server(s)

Please refer to the following architecture diagram for more details.
![cluster architecture](../infrastructure/CMSMonitoringHA.png)

## [Instructions for operators](../Operators/k8s.md)
