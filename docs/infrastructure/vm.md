# VictoriaMetrics

[VictoriaMetrics](https://github.com/VictoriaMetrics/VictoriaMetrics) (VM) is used as back-end
storage for [Prometheus](../Prometheus/README.md).

### VictoriaMetrics commands

You may use the following commands to obtain valuable information from VM:
```
# get status of underlying Time Series db:
curl http://vm-host:port/api/v1/status/tsdb

# get list of all avalable labels
curl http://vm-host:port/api/v1/labels

# get values of concrete label
curl http://vm-host:port/api/v1/label/<labe-name>/values

# get list of existing targets
curl http://vm-host:port/api/v1/targets

# extract time series data for a given attribute
curl http://vm-host:port/api/v1/export -d 'match={__name__="NAME_OF_YOUR_METRIC"}'

# extract time series data for a given pattern
curl http://vm-host:port/api/v1/export -d 'match={__name__=~".*PATTERN.*"}'
```

### VictoriaMetrics tools

The VictoriaMetrics team provides several solutions we use:

- [MetricsQL](https://victoriametrics.github.io/MetricsQL.html)
  query language inspired by PromQL. It is backwards compatible with PromQL, so
  Grafana dashboards backed by Prometheus datasource should work the same after
  switching from Prometheus to VictoriaMetrics.
- [vmagent](https://victoriametrics.github.io/vmagent.html)
  is a tiny but brave agent, which helps you collect metrics from various
  sources and stores them in VictoriaMetrics or any other Prometheus-compatible
  storage system that supports the remote_write protocol.
- [vmalert](https://victoriametrics.github.io/vmalert.html)
  executes a list of given alerting or recording rules against configured address.

## vmagent

To setup `vmagent` to securly access VictoriaMetrics we need to follow these steps:

- obtain valid token
- setup periodic update of the token
- setup `vmagent` to access VM using our valid token

Below we will discuss each step individually.

### Obtain valid token

The CMS Monitoring services are access from outside CERN network via CERN SSO
authentication mechanism. In order to access [CMS Monitoring services](services.md)
we will obtain valid token. Please follow these steps:

```
# obtain token from your favorite browser by visiting this URL
https://cms-monitoring.cern.ch/token
# the output of this page will show the following structure
AccessToken: <token>
AccessExpire: <interval in seconds>
RefreshToken: <token>
RefreshExpire: <interval in seconds>

# to proceed we need to use refresh token value
token=<RefreshToken value>
```

### setup periodic update of the token

Now we need to setup periodic update of our valid token. This can be done by
running `token-manager` daemon at your node. Grab the `auth-proxy-tools.tar.gz`
tarball from [auth-proxy releases](https://github.com/vkuznet/auth-proxy-server/releases)
area.

```
# use proper version of the tar-ball
curl -kLO https://github.com/vkuznet/auth-proxy-server/releases/download/0.1.75/auth-proxy-tools.tar.gz

# untar the tarball
tar xfz auth-proxy-tools.tar.gz

# get familiar with a tool
./token-manager -help

# start token-manager daemon, here we refresh our token every 60 seconds
# and put token content into /tmp/$USER/token area
# the $token was obtained from previous step
./token-manager -interval 60 -out /tmp/$USER/token -url https://cms-monitoring.cern.ch -token=$token
```

### setup vmagent

Please use the following steps:

```
# download vmwagent tarball from VM relase area
# https://github.com/VictoriaMetrics/VictoriaMetrics/releases
# use appropriate version for vmutils tools
curl -kLO https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v1.53.1/vmutils-amd64-v1.53.1.tar.gz

# get familiar with vmagent tool
./vmagent-prod -help

# start vmagent tool with appropriate configuration, the vmagent uses
# prometheus configuration, see 
# https://prometheus.io/docs/prometheus/latest/configuration/configuration/

# to avoid missing CERN CA, we pass tlsInsecureSkipVerify option
./vmagent-prod --remoteWrite.bearerToken=<token> \
    -promscrape.config=/path/to/prometheus.yml \
    -remoteWrite.url=https://cms-monitoring.cern.ch/ha1/victoria-metrics/api/v1/write
    -remoteWrite.tlsInsecureSkipVerify

# or use proper tls CA file for CERN identity
./vmagent-prod --remoteWrite.bearerToken=<token> \
    -promscrape.config=/path/to/prometheus.yml \
    -remoteWrite.url=https://cms-monitoring.cern.ch/ha1/victoria-metrics/api/v1/write
    -remoteWrite.tlsCAFile /etc/grid-security/certificates/CERN-GridCA.pem
```
Here is an example of `prometheus.yml` configuration file which can be used
with `vmagent`.
```
# Prometheus configuration is discussed here:
# https://prometheus.io/docs/prometheus/latest/configuration/configuration/
# In here, we'll use myapp as example of the app running on localhost:9090
global:
  scrape_interval:     15s
  evaluation_interval: 15s
scrape_configs:
  - job_name: 'myapp'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9090']
```
The `myapp` is an example of the http exporter accessible at `localhost:9090/metrics`
which can provide metrics about your service. For instance, you may run
[node_exporter](https://github.com/prometheus/node_exporter)
which you may run on your node and it will provide metrics about your node activity
(like CPU, memory utilization). Through this setup you can **push** metrics to CMS
Monitoring infrastructure.

To test the `vmagent` and your metric you can do the following:
```
# check the vmagent itself, from the same node where you run it just call this:
curl -kv http://localhost:8429/api/v1/targets
```
it should show the prometeus targets it scrapes. Then, check the metrics
in VM instance, e.g.
```
# I used http_exporter which produces vktest_status metric, therefore
# I can query VM instance for it
curl -G 'http://cms-monitoring.cern.ch:30428/api/v1/export' -d 'match={__name__="vktest_status"}'
```

Finally, you may create a plot in grafana and use
`cmsmonitoring-victoriametrics` data-source to plot your metric.
