# VMBackup

The [vmbackup](https://docs.victoriametrics.com/vmbackup.html) creates VictoriaMetrics data backups from instant snapshots. We store backups to S3. We created a new tool [vmbackup-utility](https://github.com/dmwm/CMSKubernetes/tree/master/docker/vmbackup-utility) for this purpose. We run [vmbackup-utility](https://github.com/dmwm/CMSKubernetes/tree/master/docker/vmbackup-utility) as a sidecar of for victoria-metrics [here](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/monitoring/services/agg/victoria-metrics.yaml#L54-L60)

The manual command to run vmbackup is listed below:

- `AWS_DEFAULT_REGION=CERN  /data/VictoriaMetrics/bin/vmbackup -storageDataPath=/tsdb -snapshotName=<local-snapshot> -credsFilePath=~/s3-keys -customS3Endpoint=https://s3.cern.ch -dst=s3://cms-monitoring/vmbackup/2021/05/31`

The snapshot is created using command `curl http://cms-monitoring-agg:31428/snapshot/create`. The whole procedure is setup in [vmbackup-utility](https://github.com/dmwm/CMSKubernetes/tree/master/docker/vmbackup-utility). 

We setup a cron that runs on hourly basis and send vmbackup to S3 bucket. The S3 bucket exist in https://openstack.cern.ch/project/containers/container/cms-monitoring path. 








