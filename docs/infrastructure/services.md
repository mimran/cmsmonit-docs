# CMS Monitoring services

Here we present the full list of CMS Monitoring services along with their
appropriate port numbers.

## HA services

Services which are running in High-Availability mode, please use
either `ha1` or `ha2` in appropriate URL. Please note, that within
CERN network you can use plain access to these services using appropriate
port, while outside of CERN network you can access them via
`cms-monitoring.cern.ch` entry point which requires valid token authentication:

 - *[Prometheus](../Prometheus/README.md)* keeps all CMS Monitoring metrics
    * internal: `http://cms-monitoring-ha1.cern.ch:30090`
    * external: `https://cms-monitoring.cern.ch/ha1/prometheus`

 - *[AlertManager](alertmanager.md)* keeps all CMS Monitoring alerts
    * internal: `http://cms-monitoring-ha1.cern.ch:30093`
    * external: `https://cms-monitoring.cern.ch/ha1/alertmanager`

 - [*karma*](../AlertManagement/README.md#karma-dashboard) is dashboard for AlertManager
    * internal: `http://cms-monitoring-ha1.cern.ch:30080`
    * external: `https://cms-monitoring.cern.ch/ha1/karma`

 - [*VictoriaMetrics*](vm.md) is the Prometheus back-end
    * internal: `http://cms-monitoring-ha1.cern.ch:30428`
    * external: `https://cms-monitoring.cern.ch/ha1/victoria-metrics`

 - *httpgo* keeps logs of all CMS monitoring services
    * internal: `http://cms-monitoring-ha1.cern.ch:30080`
    * external: `https://cms-monitoring.cern.ch/ha1/karma`

 - [*cmsmon-int*](../AlertManagement/README.md) for the intelligent monitoring service
 - [*ggus*](../AlertManagement/README.md#ggus-alerting-service) and [*ssb*](../AlertManagement/README.md#ssb-alerting-service) alerts scrape corresponding tickets from GGUS and SSB systems and wrap them up into alerts for AlertManager.
 - [*http-exporters*](../Exporters/README.md) we run several HTTP exporters, e.g. HDFS, EOS, and other exporters


## Non-HA services

 - *auth-proxy-server* handles access to `https://cms-monitoring.cern.ch`
    * ports: 443
 - *promxy* handles proxy access to HA services
    * ports: 30082
 - *sqoop* handles our [Sqoop jobs](../MONIT/sources.md#additional-data-sources-from-sqoop-jobs)
 - *kerberos cron* handles kerberos tickets
 - *proxy cron* handles x509 proxies
 - [*rumble*](../Rumble/README.md) provides access to HDFS via Rumble Jsoniq language
 - [*nats*](../NATS/nats.md) subscribers monitor various NATS channels
 - *log-clustering* application handles ML based clusterization of FTS accesses
