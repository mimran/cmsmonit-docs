# AlertManager 

We use the [AlertManager](https://www.prometheus.io/docs/alerting/latest/alertmanager/) (AM) service to collect all alerts from metrics scraped by Prometheus. Please refer to the
[architecture](README.md#architectural-diagram) diagram to see how we run AM services.

Here we present overview of our rules and tests used by AM services.
The AM rules define alerts for a given service. We keep all rules
and associated tests in
[gitlab.cern.ch/cms-monitoring/secrets](https://gitlab.cern.ch/cms-monitoring/secrets)
repository. A typical
[rule](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/monitoring/prometheus/rules/dbs.rules)
defines records and alerts. The latter one can define specific
alert condition, labels, annotations and evaluation period.
For more information please refer to the official AlertManager
[configuration](https://prometheus.io/docs/alerting/latest/configuration/).

The [CMS Monitoring group](https://cmsmonitoringgroup.slack.com/) on Slack provides
access to all monitoring alerts in different channels.
