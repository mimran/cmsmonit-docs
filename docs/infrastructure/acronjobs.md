# Acronjobs in CMS MONIT


The acron facility is an AFS-aware extension of the cron daemon. Acron equips the environment with Kerberos credentials and an AFS token on the user's behalf. For explanation of `acrontab` command and authentication, please see [acrontab](https://information-technology.web.cern.ch/services/fe/afs/howto/authenticate-processes).

As CMS MONIT, we run acronjobs under `cmsmonit` user. Login to `cmsmonit@lxplus.cern.ch` and run `acrontab -l` to see all acronjobs. `$HOME` is `/afs/cern.ch/user/c/cmsmonit` in all below cronjobs.



## Accronjobs
 1.  **cronPopularityPlot**:
     - Run as `/bin/bash $HOME/CMSMonitoring/scripts/cronPopularityPlot.sh --outputFormat png --outputFolder /eos/user/c/cmsmonit/www/popPlot`
     - Output: [cmsdatapop popPlot](https://cmsdatapop.web.cern.ch/cmsdatapop/popPlot/)
     - Usage in Grafana: *Latest - ScrutinyPlot* in [Data Popularity Dashboard](https://monit-grafana.cern.ch/d/o261lB1Wz/data-popularity?orgId=11).
     - See explanations about [CMS Data Popularity](https://cmsmonit-docs.web.cern.ch/Operators/popularity/#dataset-popularity-based-on-dbs-phedex-and-condor-dataset)
 2.  **cron_genCRSGplots**:
     - Run as `/bin/bash $HOME/CMSSpark/bin/cron_genCRSGplots.sh /eos/user/c/cmsmonit/www/EventCountPlots`
     - Output: [cmsdatapop EventCountPlots](https://cmsdatapop.web.cern.ch/cmsdatapop/EventCountPlots/)
     - Usage in Grafana: *Event count plots* in [C-RSG Plots Dashboard](https://monit-grafana.cern.ch/d/CkzjpJwWk/c-rsg-plots?orgId=11).
     - See explanation about [CMS Data Popularity](https://cmsmonit-docs.web.cern.ch/Operators/popularity/#event-count-plots)
 3.  **cron_EOSdataset**:
     - Run as `/bin/bash $HOME/CMSSpark/bin/cron_EOSdataset.sh /eos/user/c/cmsmonit/www/EOS/data`
     - Output: [cmsdatapop EOS data](https://cmsdatapop.web.cern.ch/cmsdatapop/EOS/data/)
     - Usage in Grafana: *EOS Popularity* in [Data Popularity Dashboard](https://monit-grafana.cern.ch/d/o261lB1Wz/data-popularity?orgId=11).
     - See explanation about [EOS Popularity](https://cmsmonit-docs.web.cern.ch/Operators/popularity/#eos-popularity)
 4.  **cron_CRABpopularity**
     - Generates datasets and plots for CRAB data popularity based on condor data in hdfs. Uses [python script](https://github.com/dmwm/CMSSpark/blob/master/src/python/CMSSpark/dbs_hdfs_crab.py)
     - Run as `/bin/bash $HOME/CMSSpark/bin/cron_CRABpopularity.sh /eos/user/c/cmsmonit/www/crabPop/data`
     - Output: [cmsdatapop crabPop](https://cmsdatapop.web.cern.ch/cmsdatapop/crabPop/)
     - Usage in Grafana: *CRAB Popularity* in [Data Popularity Dashboard](https://monit-grafana.cern.ch/d/o261lB1Wz/data-popularity?orgId=11).
 5.  **generatePNG**
     - Creates png images from all html files under `/eos/user/c/cmsmonit/www/` , html files are output of *cron_condor_cpu_efficiency* acronjob. Uses [phantomjs-binary v2.1.3](https://pypi.org/project/phantomjs-binary/) with `/afs/cern.ch/user/c/cmsmonit/convertToPNG/rasterize.js` script.
     - Run as `/bin/bash $HOME/convertToPNG/generatePNG.sh`
     - Output: Any png file under [cmsdatapop cpu_eff](https://cmsdatapop.web.cern.ch/cmsdatapop/cpu_eff/)
 6.  **cron_hs06cputime_plot**
     - Generates datasets and plots for HS06 core hours either by week of year or by month for a given calendar year.
     - Run as `/bin/bash $HOME/CMSSpark/bin/cron_hs06cputime_plot.sh /eos/home-c/cmsmonit/www/hs06cputime/`
     - Output: [cmsdatapop hs06cputime](https://cmsdatapop.web.cern.ch/cmsdatapop/hs06cputime/)
     - Usage in Grafana: *Fig2* and *Fig4* in [C-RSG Plots Dashboard](https://monit-grafana.cern.ch/d/CkzjpJwWk/c-rsg-plots?orgId=11).
 7.  **cron_crab_unique_users**
     - Generates datasets and plots for CRAB unique users count either by week of year or by month for a given calendar year.
     - Run as `/bin/bash $HOME/CMSSpark/bin/cron_crab_unique_users.sh /eos/home-c/cmsmonit/www/crab_uu/`
     - Output: [cmsdatapop crab_uu](https://cmsdatapop.web.cern.ch/cmsdatapop/hs06cputime/crab_uu)
     - Usage in Grafana: *Fig5* in [C-RSG Plots Dashboard](https://monit-grafana.cern.ch/d/CkzjpJwWk/c-rsg-plots?orgId=11).
 8.  **cron_condor_cpu_efficiency**
     - Generates a static site with information about workflows/requests cpu efficiency for the workflows/request matching the parameters. *CPU_Efficiency_Table.html* tables exist under each cms job type(production, analysis, etc.) folder.
     - Run as `/bin/bash $HOME/CMSSpark/bin/cron_condor_cpu_efficiency.sh /eos/home-c/cmsmonit/www/cpu_eff`
     - Output: [cmsdatapop cpu_eff](https://cmsdatapop.web.cern.ch/cmsdatapop/cpu_eff/)

