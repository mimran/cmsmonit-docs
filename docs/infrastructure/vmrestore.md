The `vmrestore` restores data from backups created by `vmbackup`. 

We setup a script that restores vmbackup from S3 bucket available at [victoria-metrics-restore](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/monitoring/scripts/victoria-metrics-restore.sh)

The VictoriaMetrics must be stopped during the restore process.

The script first delete victoriametrics pod and create `vmrestore` job that restore backup. After backup is 
completed, the `vmrestore` job is deleted and victoriametrics pod is started again. 

The usage instructions of the script is as follows:

- `Usage Instructions: <complete bucket-path> <backup service-name>`

For victoria-metrics, we can use command like this:

- `./victoria-metrics-restore.sh cms-monitoring/vmbackup/2021/05/31/00:00 victoria-metrics`

For victoria-metrics-long, we can use command like this:

- `./victoria-metrics-restore.sh cms-monitoring/vmbackup-long/2021/05/31/00:00 victoria-metrics-long`

