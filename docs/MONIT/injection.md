# How to inject data into MONIT

Please read first the [documentation](https://monit-docs.web.cern.ch/metrics/) of the CERN MONIToring infrastructure. 
Most CMS producers use CERN ActiveMQ (AMQ) message passing described [here](https://monit-docs.web.cern.ch/metrics/amq/).

In order to add your data to MONIT follow these steps:
 
 1.  Actions from your team:
     1. provide a sample of document which you'll intend to feed into CERN MONIT system. The data should be in JSON data-format.
     2. provide an estimate of data rates and volume you anticipate to feed into CERN MONIT system
 2.  Actions from CERN MONIT team:
     1. we need to request from them an entry point and collection where you'll send your data
     2. they will ask for the following information:
         1. What is the description of the type of data you are sending?
            - we suggest to use something like monit_prod_cms_yourproject
         2. Do you have suggestions or preferences for the topic name?
            - use something like topic/cms_yourproject
         3. Which email address (preferably an e-group) should be listed as responsible for the data producer?
            - this is not very high traffic, feel free to use or create your alert/ops egroup
         4. How will you authenticate to the brokers (certificate or regular account)?
            - in our previous topics we used user/password authentication. The MONIT team provided us with credentials.
         5. Who (i.e. which authenticated principals) should be authorized to send messages? to consume message?
            - in previous requests we explicitly listed nodes which will send the data, and a common account which is used to push the data. The consumer will be the CERN MONIT team.
    3. once the entry point is created and you'll get your credentials and URLs, they will ask you to send few docs to verify the data flow.
 3. Actions from your team:
     1. you'll need to send data into CERN MONIT end-point via StompAMQ. The module exists in WMCore area, 
  see [StompAMQ.py module](https://github.com/dmwm/CMSMonitoring/tree/master/src/python/CMSMonitoring)
  and you may have a look at example how WMArchive does it [here](https://github.com/dmwm/WMArchive/blob/master/src/python/WMArchive/Tools/myspark.py#L425-L440),
  [here](https://github.com/dmwm/WMArchive/blob/master/src/python/WMArchive/Tools/myspark.py#L348-L356),
  [here](https://github.com/dmwm/WMArchive/blob/master/src/python/WMArchive/Tools/myspark.py#L348-L356) and 
  [here](https://github.com/dmwm/WMArchive/blob/master/src/python/WMArchive/Tools/myspark.py#L44).
     2. Basically, you'll load StompAMQ module, parse your credential json file (CERN MONIT will give it to you) and grab
  your docs, wrap them up and send to MONIT via AMQ. In order to use StompAMQ module you'll need to load [py-stomp](https://pypi.org/project/stomp.py/)
  package which is available at PyPi  or use py2-stomp RPM from CMS comp repository, see [py2-stomp.spec](https://github.com/cms-sw/cmsdist/blob/comp_gcc630/py2-stomp.spec).
     3. Each data-provider should provide a document describing their data attributes. A good example is [HTCondor](https://github.com/dmwm/cms-htcondor-es/blob/master/README.md).
     4. The code used to feed data into MONIT should reside in some cms common repository (for example dmwm or cmssw)
     5. you'll need to find your data in Kibana/Grafana and verify their content
        - we can help you to find your data in [Kibana](Kibana.md) and show how to create a plot
        - we can help you to find your data in [Grafana](Grafana.md) and create first dashboard
 4.  Actions from your team:
     1. you'll start feeding the data on a regular basis
     2. provide links to code and documentation of attributes/schema
     3. you'll create/support necessary dashboards to visualize your data
 
By the end of this process we'll include your dashboard into the central CMS Monitoring web page.

Once your code is in production, you may use [this](https://github.com/dmwm/cms-htcondor-es/wiki/CMSMonitoring-Data-Flow-Test-procedure)
procedure to test new versions of the code.
