<span style="color:#4169E1">**Name:** AccountingGroup </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** AffiliationCountry </span>
<br>**Mapping:** None 
<br>**Definition:** AffiliationCountry = country field in the affiliation of CRAB_UserHN or x509userproxysubject 
<br>**Description:** A cronjob updates affiliations in our static file hourly. Spider code maps <br>affiliation information with the keys CRAB_UserHN or x509userproxysubject. <br>AffiliationCountry and AffiliationInstitute comes from cms-cric-dev.cern.ch 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** AffiliationInstitute </span>
<br>**Mapping:** None 
<br>**Definition:** AffiliationInstitute = institute field in the affiliation of CRAB_UserHN or x509userproxysubject 
<br>**Description:** A cronjob updates affiliations in our static file hourly. Spider code maps <br>affiliation information with the keys CRAB_UserHN or x509userproxysubject. <br>AffiliationCountry and AffiliationInstitute comes from cms-cric-dev.cern.ch 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Args </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** AssignedGPUs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** AutoClusterId </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** BSCJob </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Badput </span>
<br>**Mapping:** None 
<br>**Definition:** Badput = max(CoreHr None CommittedCoreHr, 0.0) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** BenchmarkJobDB12 </span>
<br>**Mapping:** None 
<br>**Definition:** BenchmarkJobDB12 = float(MachineAttrDIRACBenchmark0) 
<br>**Description:** An estimate of the per-core performance of the machine the job last ran on, <br>based on the DB12 benchmark. Higher is better. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** BenchmarkJobHS06 </span>
<br>**Mapping:** None 
<br>**Definition:** BenchmarkJobHS06 = float(MachineAttrMJF_JOB_HS06_JOB0) / cpus 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** BlockReadBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** BlockWriteBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** BytesRecvd </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** BytesSent </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CHIRP_JOB_Exit_Status </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSGroups </span>
<br>**Mapping:** None 
<br>**Definition:** CMSGroups = make_list_from_string_field(ad, CMSGroups) 
<br>**Description:** Name of the CMS group associated with this request (production only). Example; HIG. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSPrimaryDataTier </span>
<br>**Mapping:** None 
<br>**Definition:** CMSPrimaryDataTier = str(DESIRED_CMSDataset).split("/")[-1] 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSPrimaryPrimaryDataset </span>
<br>**Mapping:** None 
<br>**Definition:** CMSPrimaryPrimaryDataset = str(DESIRED_CMSDataset).split("/")[1] 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSPrimaryProcessedDataset </span>
<br>**Mapping:** None 
<br>**Definition:** CMSPrimaryProcessedDataset = str(DESIRED_CMSDataset).split("/")[2] 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSProcessingSiteName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSWDone </span>
<br>**Mapping:** None 
<br>**Definition:** CMSSWDone = bool(ChirpCMSSWDone) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSWEventRate </span>
<br>**Mapping:** None 
<br>**Definition:** CMSSWEventRate = (ChirpCMSSWEvents OR 0) / float(ChirpCMSSWElapsed * (RequestCpus OR 1.0)) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSWKLumis </span>
<br>**Mapping:** None 
<br>**Definition:** CMSSWKLumis = ChirpCMSSWLumis / 1000.0 
<br>**Description:** The number of lumi sections processed by the job, in thousands. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSWMajorVersion </span>
<br>**Mapping:** None 
<br>**Definition:** Please see following lines of the comment, "Parse CRAB3 information on CMSSW version" 
<br>**Description:** Unknown or regex result 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSWReleaseSeries </span>
<br>**Mapping:** None 
<br>**Definition:** Please see following lines of the comment, Parse CRAB3 information on CMSSW version 
<br>**Description:** Unknown or regex result 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSWTimePerEvent </span>
<br>**Mapping:** None 
<br>**Definition:** CMSSWTimePerEvent = 1.0 / CMSSWEventRate 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSWVersion </span>
<br>**Mapping:** None 
<br>**Definition:** Please see following lines of the comment 
<br>**Description:** Unknown or regex result 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSWWallHrs </span>
<br>**Mapping:** None 
<br>**Definition:** CMSSWWallHrs = ChirpCMSSWElapsed / 3600.0 
<br>**Description:** The number of wall hours reported by cmsRun. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSW_Versions </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSite </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMSSubsiteName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMS_CampaignType </span>
<br>**Mapping:** None 
<br>**Definition:** CMS_CampaignType = guess_campaign_type(ad, analysis) 
<br>**Description:** analysis = (Type == "analysis" OR CMS_JobType == "Analysis") 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMS_JobRetryCount </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMS_JobType </span>
<br>**Mapping:** None 
<br>**Definition:** CMS_JobType = str((CMS_JobType OR "Analysis") if analysis else "Unknown")) 
<br>**Description:** analysis = (Type == "analysis" OR CMS_JobType == "Analysis") 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMS_Pool </span>
<br>**Mapping:** None 
<br>**Definition:** CMS_Pool = pool_name 
<br>**Description:** Condor pool to which the scheduler belongs. e.g. Global, Volunteer, Tier0. <br>pool_name comes from our static list of collectors file. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMS_SubmissionTool </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMS_TaskType </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMS_Type </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CMS_WMTool </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CPUModel </span>
<br>**Mapping:** None 
<br>**Definition:** CPUModel = str(ChirpCMSSWCPUModels) OR str(MachineAttrCPUModel0) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CPUModelName </span>
<br>**Mapping:** None 
<br>**Definition:** CPUModelName = str(ChirpCMSSWCPUModels) OR str(MachineAttrCPUModel0) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CPUsUsage </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_AlgoArgs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_AsyncDest </span>
<br>**Mapping:** None 
<br>**Definition:** CRAB_AsyncDest = str(CRAB_AsyncDest) OR "Unknown" 
<br>**Description:** The output destination CMS site name for a CRAB3 job. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_DataBlock </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** The primary input block name. For CRAB3 jobs only. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_HC </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_ISB </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_Id </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_JobArch </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_JobCount </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_JobLogURL </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_JobReleaseTimeout </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_JobSW </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_NoWNStageout </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_OutLFNDir </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_PostJobLastUpdate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_PostJobLogURL </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_PostJobStatus </span>
<br>**Mapping:** None 
<br>**Definition:** Please see following lines of the comment; 
<br>**Description:** Please read the comments 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_PrimaryDataset </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_Publish </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_PublishName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_Retry </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_SaveLogsFlag </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_SiteBlacklist </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_SiteWhitelist </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_SplitAlgo </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_SubmitterIpAddr </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_TaskCreationDate </span>
<br>**Mapping:** None 
<br>**Definition:** CRAB_TaskCreationDate = get_creation_time_from_taskname(ad) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_TaskEndTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_TaskLifetimeDays </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_TaskWorker </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_TransferOutputs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_UserGroup </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_UserHN </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** For analysis jobs, CMS username of user that submitted the task. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_UserRole </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CRAB_Workflow </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** For analysis jobs, the CRAB task name. It is of the form <br>170406_201711:ztu_crab_multicrab_CMEandMixedHarmonics_Pbp_HM185_250_q3_v3_185_250_1_3; <br>for the Workflow attribute, this is shortened to <br>ztu_crab_multicrab_CMEandMixedHarmonics_Pbp_HM185_250_q3_v3_185_250_1_3. <br>CRAB_Workflow is considered unique for a single CRAB task; depending on <br>the user's naming scheme, there may multiple CRAB tasks per Workflow. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Campaign </span>
<br>**Mapping:** None 
<br>**Definition:** Campaign = guessCampaign(ad, analysis) 
<br>**Description:** The campaign this job belongs to; derived from the WMAgent workflow name <br>(so, this is an approximation; may have nonsense values <br>for strangely-named workflows). Only present for production jobs.<br>analysis = (Type == "analysis" OR CMS_JobType == "Analysis") 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWCPUModels </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWCPUSpeed </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWDone </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_Done 
<br>**Definition:** ChirpCMSSWDone = int(ChirpCMSSWDone)<br> 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWElapsed </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_Elapsed 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWEventRate </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_EventRate 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWEvents </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_Events 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWFiles </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_Files 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWLastUpdate </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_LastUpdate 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWLumis </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_Lumis 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWMaxEvents </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_MaxEvents 
<br>**Definition:** None 
<br>**Description:** The maximum number of events a job will process before exiting. This may not be <br>known for all jobs; in that case, -1 is reported. These can be used to estimate <br>the percent completion of jobs; the veracity of these attributes are not known.<br>Please see handle_chirp_info funtion and see split and append methods. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWMaxFiles </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_MaxFiles 
<br>**Definition:** None 
<br>**Description:** The maximum number of files a job will process before exiting. This may not be <br>known for all jobs; in that case, -1 is reported. These can be used to estimate <br>the percent completion of jobs; the veracity of these attributes are not known.<br>Please see handle_chirp_info funtion and see split and append methods. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWMaxLumis </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_MaxLumis 
<br>**Definition:** None 
<br>**Description:** The maximum number of lumis a job will process before exiting. This may not be <br>known for all jobs; in that case, -1 is reported. These can be used to estimate <br>the percent completion of jobs; the veracity of these attributes are not known.<br>Please see handle_chirp_info funtion and see split and append methods. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWReadBytes </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_ReadBytes 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWReadOps </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_ReadOps 
<br>**Definition:** None 
<br>**Description:** Number of read operations performed (excludes readv activity). <br>Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWReadSegments </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_ReadSegments 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWReadTimeMsecs </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_ReadTimeMsecs 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWReadVOps </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_ReadVOps 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWRuns </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_Runs 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWTotalCPU </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_TotalCPU 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWWriteBytes </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_WriteBytes 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSWWriteTimeMsecs </span>
<br>**Mapping:** ChirpCMSSW_cmsRun(1,2,3,4,5,6)_WriteTimeMsecs 
<br>**Definition:** None 
<br>**Description:** Please see handle_chirp_info funtion and see split and append methods 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_SiteIO.ChirpString </span>
<br>**Mapping:** None 
<br>**Definition:** handle_chirp_info, chirpCMSSWIOSiteName 
<br>**Description:** Please see new objects siteio 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_SiteIO.ReadBytes </span>
<br>**Mapping:** None 
<br>**Definition:** handle_chirp_info, chirpCMSSWIOSiteName 
<br>**Description:** Please see new objects siteio 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_SiteIO.ReadTimeMS </span>
<br>**Mapping:** None 
<br>**Definition:** handle_chirp_info, chirpCMSSWIOSiteName 
<br>**Description:** Please see new objects siteio 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_SiteIO.SiteName </span>
<br>**Mapping:** None 
<br>**Definition:** handle_chirp_info, chirpCMSSWIOSiteName 
<br>**Description:** Please see new objects siteio 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_Done </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_Elapsed </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_EventRate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_Events </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_Files </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_LastUpdate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_Lumis </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_MaxEvents </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_MaxFiles </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_MaxLumis </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_ReadBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_ReadOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_ReadSegments </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_ReadTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_ReadVOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_Runs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_TotalCPU </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_WriteBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun1_WriteTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_Done </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_Elapsed </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_EventRate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_Events </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_Files </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_LastUpdate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_Lumis </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_MaxFiles </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_ReadBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_ReadOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_ReadSegments </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_ReadTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_ReadVOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_Runs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_TotalCPU </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_WriteBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun2_WriteTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_Done </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_Elapsed </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_EventRate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_Events </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_Files </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_LastUpdate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_Lumis </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_MaxFiles </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_ReadBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_ReadOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_ReadSegments </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_ReadTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_ReadVOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_Runs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_TotalCPU </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_WriteBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun3_WriteTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_Done </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_Elapsed </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_EventRate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_Events </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_Files </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_LastUpdate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_Lumis </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_MaxFiles </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_ReadBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_ReadOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_ReadSegments </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_ReadTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_ReadVOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_Runs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_TotalCPU </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_WriteBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun4_WriteTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_Done </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_Elapsed </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_EventRate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_Events </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_Files </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_LastUpdate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_Lumis </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_MaxFiles </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_ReadBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_ReadOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_ReadSegments </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_ReadTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_ReadVOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_Runs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_TotalCPU </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_WriteBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun5_WriteTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_Done </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_Elapsed </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_EventRate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_Events </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_Files </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_LastUpdate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_Lumis </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_MaxFiles </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_ReadBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_ReadOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_ReadSegments </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_ReadTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_ReadVOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_Runs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_TotalCPU </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_WriteBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun6_WriteTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_Done </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_Elapsed </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_EventRate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_Events </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_Files </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_IOSite_T2_US_Florida_ReadTimeMS </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_IOSite_cernch_ReadTimeMS </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_IOSite_indiacmsresin_ReadTimeMS </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_IOSite_physikrwthaachende_ReadTimeMS </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_LastUpdate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_Lumis </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_MaxEvents </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_MaxFiles </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_MaxLumis </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_ReadBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_ReadOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_ReadSegments </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_ReadTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_ReadVOps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_Runs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_TotalCPU </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_WriteBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ChirpCMSSW_cmsRun_WriteTimeMsecs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_CRAB3_Job_ExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun1_Exception_Message </span>
<br>**Mapping:** None 
<br>**Definition:** let x := Chirp_WMCore_cmsRun(1,2,3,4,5,6)_Exception_Message<br>if _wmcore_exe_exmsg.match(x) then <br>x = str(decode_and_decompress(Chirp_WMCore_cmsRun1_Exception_Message)) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun1_ExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun2_Exception_Message </span>
<br>**Mapping:** None 
<br>**Definition:** let x := Chirp_WMCore_cmsRun(1,2,3,4,5,6)_Exception_Message<br>if _wmcore_exe_exmsg.match(x) then <br>x = str(decode_and_decompress(Chirp_WMCore_cmsRun1_Exception_Message)) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun2_ExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun3_Exception_Message </span>
<br>**Mapping:** None 
<br>**Definition:** let x := Chirp_WMCore_cmsRun(1,2,3,4,5,6)_Exception_Message<br>if _wmcore_exe_exmsg.match(x) then <br>x = str(decode_and_decompress(Chirp_WMCore_cmsRun1_Exception_Message)) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun3_ExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun4_Exception_Message </span>
<br>**Mapping:** None 
<br>**Definition:** let x := Chirp_WMCore_cmsRun(1,2,3,4,5,6)_Exception_Message<br>if _wmcore_exe_exmsg.match(x) then <br>x = str(decode_and_decompress(Chirp_WMCore_cmsRun1_Exception_Message)) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun4_ExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun5_Exception_Message </span>
<br>**Mapping:** None 
<br>**Definition:** let x := Chirp_WMCore_cmsRun(1,2,3,4,5,6)_Exception_Message<br>if _wmcore_exe_exmsg.match(x) then <br>x = str(decode_and_decompress(Chirp_WMCore_cmsRun1_Exception_Message)) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun5_ExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun6_Exception_Message </span>
<br>**Mapping:** None 
<br>**Definition:** let x := Chirp_WMCore_cmsRun(1,2,3,4,5,6)_Exception_Message<br>if _wmcore_exe_exmsg.match(x) then <br>x = str(decode_and_decompress(Chirp_WMCore_cmsRun1_Exception_Message)) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun6_ExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun7_Exception_Message </span>
<br>**Mapping:** None 
<br>**Definition:** let x := Chirp_WMCore_cmsRun(1,2,3,4,5,6)_Exception_Message<br>if _wmcore_exe_exmsg.match(x) then <br>x = str(decode_and_decompress(Chirp_WMCore_cmsRun1_Exception_Message)) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun7_ExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun_Exception_Message </span>
<br>**Mapping:** None 
<br>**Definition:** let x := Chirp_WMCore_cmsRun(1,2,3,4,5,6)_Exception_Message<br>if _wmcore_exe_exmsg.match(x) then <br>x = str(decode_and_decompress(Chirp_WMCore_cmsRun1_Exception_Message)) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Chirp_WMCore_cmsRun_ExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ClusterId </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Cmd </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CommittedCoreHr </span>
<br>**Mapping:** None 
<br>**Definition:** CommittedCoreHr = (RequestCpus OR 1.0) * (CommittedTime OR 0) / 3600.0 
<br>**Description:** The core-hours only for the last run of the job (excluding preempted attempts). 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CommittedSlotTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CommittedSuspensionTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CommittedTime </span>
<br>**Mapping:** None 
<br>**Definition:** if JobStatus == 2 AND ((EnteredCurrentStatus OR now + 1) < now); then<br>  CommittedTime = int(now None EnteredCurrentStatus)<br><br>if CRAB_PostJobStatus AND ((Status == "Removed" AND CRAB_PostJobStatus != "NOT RUN") OR (Status == "Completed")) AND (CommittedTime OR 0) == 0; then<br>  CommittedTime"] = RemoteWallClockTime 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CommittedWallClockHr </span>
<br>**Mapping:** None 
<br>**Definition:** CommittedWallClockHr = (CommittedTime OR 0) / 3600.0 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CompletedFiles </span>
<br>**Mapping:** None 
<br>**Definition:** CompletedFiles = ChirpCMSSWFiles 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CompletionDate </span>
<br>**Mapping:** None 
<br>**Definition:** CompletionDate = EnteredCurrentStatus 
<br>**Description:** see if clause! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CondorExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** CondorExitCode = ExitCode 
<br>**Description:** see if clause! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ConnectClientGeoData </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ConnectClientLocalDir </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ConnectClientLocalNode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ConnectClientLocalUser </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ConnectClientServer </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ConnectClientUser </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ConnectClientVersion </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ConnectWrapper </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CoreHr </span>
<br>**Mapping:** None 
<br>**Definition:** CoreHr = ((RequestCpus OR 1.0) * int(RemoteWallClockTime OR 0)) / 3600.0) 
<br>**Description:** The number of core-hours utilized by the job. If the job lasted for 24 hours and utilized 4 cores, <br>the value of CoreHr will be 96. This includes all runs of the job, <br>including any time spent on preempted instance. Please see derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CoreSize </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Country </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** Country where the job ran (for example, CH). 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CpuBadput </span>
<br>**Mapping:** None 
<br>**Definition:** CpuBadput = max(CoreHr None CpuTimeHr, 0.0) 
<br>**Description:** The badput associated with a job, in hours. This is the sum of all unsuccessful <br>job attempts. If a job runs for 2 hours, is preempted, restarts, <br>then completes successfully after 3 hours, the CpuBadput is 2. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CpuEff </span>
<br>**Mapping:** None 
<br>**Definition:** CpuEff = (100 * CpuTimeHr / WallClockHr / float(RequestCpus OR 1.0)) 
<br>**Description:** The total scheduled CPU time (user and system CPU) divided by core hours, <br>in percentage. If the job lasted for 24 hours, utilized 4 cores, <br>and used 72 hours of CPU time, then CpuEff would be 75. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CpuTimeHr </span>
<br>**Mapping:** None 
<br>**Definition:** CpuTimeHr = ((RemoteSysCpu OR 0) + (RemoteUserCpu OR 0)) / 3600.0 
<br>**Description:** The amount of CPU time (sum of user and system) attributed to the job, in hours. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CpusProvisioned </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CronDayOfMonth </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CronDayOfWeek </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CronHour </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CronMinute </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CronMonth </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CumulativeRemoteSysCpu </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CumulativeRemoteUserCpu </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CumulativeSlotTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CumulativeSuspensionTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CurrentHosts </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** CurrentStatusUnknown </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DAGNodeName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DAGParentNodeNames </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DB12CommittedCoreHr </span>
<br>**Mapping:** None 
<br>**Definition:** DB12CommittedCoreHr = (CommittedCoreHr * BenchmarkJobDB12) 
<br>**Description:** Please see derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DB12CoreHr </span>
<br>**Mapping:** None 
<br>**Definition:** DB12CoreHr = CoreHr * BenchmarkJobDB12 
<br>**Description:** Please see derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DB12CpuTimeHr </span>
<br>**Mapping:** None 
<br>**Definition:** DB12CpuTimeHr = CpuTimeHr * BenchmarkJobDB12 
<br>**Description:** Please see derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DB12EventRate </span>
<br>**Mapping:** None 
<br>**Definition:** DB12EventRate = EventRate / BenchmarkJobDB12 
<br>**Description:** Please see derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DB12TimePerEvent </span>
<br>**Mapping:** None 
<br>**Definition:** DB12TimePerEvent = (TimePerEvent * BenchmarkJobDB12) 
<br>**Description:** Please see derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DESIRED_Archs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DESIRED_CMSDataLocations </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DESIRED_CMSDataset </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** The primary input dataset name. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DESIRED_CMSPileups </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DESIRED_Entries </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DESIRED_Overflow_Region </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DESIRED_SITES_Diff </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DESIRED_SITES_Orig </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DESIRED_Sites </span>
<br>**Mapping:** None 
<br>**Definition:** DESIRED_Sites = make_list_from_string_field(ad, DESIRED_Sites) 
<br>**Description:** The list of sites the job potentially could run on. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DataCollection </span>
<br>**Mapping:** None 
<br>**Definition:** DataCollection = (CompletionDate OR 0) OR _launch_time 
<br>**Description:** _launch_time is start time of the script 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DataCollectionDate </span>
<br>**Mapping:** None 
<br>**Definition:** DataCollectionDate = RecordTime 
<br>**Description:** When the job exited the queue or when the JSON document was last updated, <br>whichever came first. Use this field for time-based queries. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DataLocations </span>
<br>**Mapping:** None 
<br>**Definition:** DataLocations = make_list_from_string_field(ad, DESIRED_CMSDataLocations) 
<br>**Description:** The list of known primary input dataset locations. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DataLocationsCount </span>
<br>**Mapping:** None 
<br>**Definition:** DataLocationsCount = len(DataLocations) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DeferralPrepTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DeferralTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DeferralWindow </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DelegatedProxyExpiration </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DesiredOS </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DesiredSiteCount </span>
<br>**Mapping:** None 
<br>**Definition:** DesiredSiteCount = len(DESIRED_Sites) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DiskProvisioned </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DiskUsage </span>
<br>**Mapping:** DiskUsage_RAW 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** DiskUsageGB </span>
<br>**Mapping:** None 
<br>**Definition:** DiskUsageGB = (DiskUsage_RAW OR 0) / 1000000.0 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** EditedClusterAttrs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** EncryptExecuteDirectory </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** EnteredCurrentStatus </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ErrorClass </span>
<br>**Mapping:** None 
<br>**Definition:** ErrorClass = errorClass(result) 
<br>**Description:** Please see the function! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ErrorType </span>
<br>**Mapping:** None 
<br>**Definition:** ErrorType = errorType(ad) 
<br>**Description:** Please see the function! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** EstimatedSingleCoreMins </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** EstimatedWallTimeMins </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** EventRate </span>
<br>**Mapping:** None 
<br>**Definition:** EventRate = (ChirpCMSSWEvents OR 0) / float(CoreHr * 3600.0) 
<br>**Description:** The number of events per second per core. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ExeExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ExecutableSize </span>
<br>**Mapping:** ExecutableSize_RAW 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ExitBySignal </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** ExitCode = commonExitCode(ad) 
<br>**Description:** Exit code of the job. If available, this is actually the exit code of the last CMSSW step. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ExitReason </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ExitSignal </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ExitStatus </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ExtDESIRED_Sites </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ExtraMemory </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** FERMIHTC_JobType </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** FileSystemDomain </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** FormattedCrabId </span>
<br>**Mapping:** None 
<br>**Definition:** FormattedCrabId = get_formatted_CRAB_Id(CRAB_Id) 
<br>**Description:** Please see the function! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDECLIENT_Name </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDECLIENT_Name 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_CMSSite </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_CMSSite 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_ClusterId </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_ClusterId 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_Cpus </span>
<br>**Mapping:** ?GLIDEIN_Cpus or MATCH_EXP_JOB_GLIDEIN_Cpus 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_Entry_Name </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_Entry_Name 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_Factory </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_Factory 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_Job_Max_Time </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_Job_Max_Time 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_MaxMemMBs </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_MaxMemMBs 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_Max_Walltime </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_Max_Walltime 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_Memory </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_Memory 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_Name </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_Name 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_ProcId </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_ProcId 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_ResourceName </span>
<br>**Mapping:** ?MATCH_EXP_JOB_GLIDEIN_ResourceName(bug?) 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_SEs </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_SEs 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_Schedd </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_Schedd 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_Site </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_Site 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_SiteWMS </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_SiteWMS 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_SiteWMS_JobId </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_SiteWMS_JobId 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_SiteWMS_Queue </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_SiteWMS_Queue 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_SiteWMS_Slot </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_SiteWMS_Slot 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_ToDie </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_ToDie 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GLIDEIN_ToRetire </span>
<br>**Mapping:** MATCH_EXP_JOB_GLIDEIN_ToRetire 
<br>**Definition:** None 
<br>**Description:** Please see renaming fields starts with "MATCH_EXP_JOB_" 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GPUsProvisioned </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GlobalJobId </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** Use to uniquely identify individual jobs. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GlobusResubmit </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GlobusStatus </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GridJobId </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GridJobStatus </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GridResource </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GridResourceUnavailableTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** GridpackCard </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HS06CommittedCoreHr </span>
<br>**Mapping:** None 
<br>**Definition:** HS06CommittedCoreHr = CommittedCoreHr * BenchmarkJobHS06 
<br>**Description:** Please see derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HS06CoreHr </span>
<br>**Mapping:** None 
<br>**Definition:** HS06CoreHr = CoreHr * BenchmarkJobHS06 
<br>**Description:** Please see derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HS06CpuTimeHr </span>
<br>**Mapping:** None 
<br>**Definition:** HS06CpuTimeHr = CpuTimeHr * BenchmarkJobHS06 
<br>**Description:** Please see derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HS06EventRate </span>
<br>**Mapping:** None 
<br>**Definition:** HS06EventRate = EventRate / BenchmarkJobHS06 
<br>**Description:** Please see if clause and derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HS06TimePerEvent </span>
<br>**Mapping:** None 
<br>**Definition:** HS06TimePerEvent = TimePerEvent * BenchmarkJobHS06 
<br>**Description:** Please see if clause and derived fields! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HasBeenOverflowRouted </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HasBeenRouted </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HasBeenTimingTuned </span>
<br>**Mapping:** None 
<br>**Definition:** HasBeenTimingTuned = (HasBeenTimingTuned OR False) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HasSingularity </span>
<br>**Mapping:** None 
<br>**Definition:** HasSingularity = True if MachineAttrHAS_SINGULARITY0 is True else False 
<br>**Description:** Set to true if the job was run inside a Singularity container. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HoldCopiedFromTargetJob </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HoldReason </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HoldReasonCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** HoldReasonSubCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** IOWait </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ImageSize </span>
<br>**Mapping:** ImageSize_RAW 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** InputData </span>
<br>**Mapping:** None 
<br>**Definition:** InputData = "Unknown", "Onsite", "Offsite" 
<br>**Description:** Onsite if Site is in the DataLocations list; Offsite otherwise. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** InputGB </span>
<br>**Mapping:** None 
<br>**Definition:** InputGB = ChirpCMSSWReadBytes / 1e9 
<br>**Description:** Amount of data read by the CMSSW process; in gigabytes. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** InteractiveJob </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** IoslotsProvisioned </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** IotokensProvisioned </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** IsGridpack </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** IsProxyWatcher </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JOBGLIDEIN_ResourceName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JOB_CMSSite </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JOB_GLIDEIN_Cpus </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JOB_Gatekeeper </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobBatchId </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobBatchName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobCoreDumped </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobCoreFileName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobCpus </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobCurrentFinishTransferInputDate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobCurrentFinishTransferOutputDate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobCurrentStartDate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobCurrentStartExecutingDate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobCurrentStartTransferInputDate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobCurrentStartTransferOutputDate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobDescription </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobDisconnectedDate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobDuration </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobExitCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobFailed </span>
<br>**Mapping:** None 
<br>**Definition:** JobFailed = jobFailed(ad) 
<br>**Description:** Please see the function! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobFinishedHookDone </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobFlavour </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobLastStartDate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobLeaseExpiration </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobPid </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobPrio </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobRunCount </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobStartDate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobState </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobStatus </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** JobUniverse </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** KEvents </span>
<br>**Mapping:** None 
<br>**Definition:** KEvents = ChirpCMSSWEvents / 1000.0 
<br>**Description:** The number of events processed by the job, in thousands. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** KillSig </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastHoldReason </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastHoldReasonCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastHoldReasonSubCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastJobLeaseRenewal </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastJobStatus </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastMatchTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastMaxWalltimeUpdate_JobDuration </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastRejMatchTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastReleaseReason </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastRemoteHost </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastRemoteStatusUpdate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastRouted </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastSuspensionTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LastVacateTime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LocalSysCpu </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** LocalUserCpu </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MATCH_Cpus </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MATCH_EXP_Args </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MATCH_EXP_JOBGLIDEIN_ResourceName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MATCH_EXP_Used_Gatekeeper </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MATCH_GLIDEIN_Gatekeeper </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MATCH_GLIDEIN_ToDie </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MATCH_GLIDEIN_ToRetire </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCMSSubSiteName0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCUDACapability0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** GPU related field in string type 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) - [ref2](https://htcondor.readthedocs.io/en/latest/classad-attributes/machine-classad-attributes.html) -  
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** SI, CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCUDAClockMhz0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** GPU related field 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) - [ref2](https://htcondor.readthedocs.io/en/latest/classad-attributes/machine-classad-attributes.html) -  
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** SI, CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCUDAComputeUnits0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** GPU related field 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) - [ref2](https://htcondor.readthedocs.io/en/latest/classad-attributes/machine-classad-attributes.html) -  
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** SI, CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCUDACoresPerCU0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** GPU related field 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) - [ref2](https://htcondor.readthedocs.io/en/latest/classad-attributes/machine-classad-attributes.html) -  
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** SI, CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCUDADeviceName0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** GPU related field 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) - [ref2](https://htcondor.readthedocs.io/en/latest/classad-attributes/machine-classad-attributes.html) -  
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** SI, CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCUDADriverVersion0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** GPU related field in string type 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) - [ref2](https://htcondor.readthedocs.io/en/latest/classad-attributes/machine-classad-attributes.html) -  
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** SI, CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCUDAECCEnabled0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** GPU related field 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) - [ref2](https://htcondor.readthedocs.io/en/latest/classad-attributes/machine-classad-attributes.html) -  
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** SI, CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCUDAGlobalMemoryMb </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** GPU related field 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) - [ref2](https://htcondor.readthedocs.io/en/latest/classad-attributes/machine-classad-attributes.html) -  
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** SI, CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCpus0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCpus1 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCpus2 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCpus3 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCpus4 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCpus5 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCpus6 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCpus7 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrCpus8 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrDIRACBenchmark0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_CMSSite1 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_CMSSite2 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_CMSSite3 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_CMSSite4 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_CMSSite5 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_CMSSite6 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_CMSSite7 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_CMSSite8 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_Gatekeeper0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_ToDie0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_ToDie1 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_ToDie2 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_ToDie3 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_ToDie4 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_ToDie5 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_ToDie6 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_ToDie7 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrGLIDEIN_ToDie8 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrHAS_SINGULARITY0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrMJF_JOB_HS06_JOB0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrMaxHibernateTime0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrSlotWeight0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrSlotWeight1 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrSlotWeight2 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrSlotWeight3 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrSlotWeight4 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrSlotWeight5 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrSlotWeight6 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrSlotWeight7 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrSlotWeight8 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MachineAttrTotalSlotCpus0 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ManagedManager </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MaxCores </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MaxFiles </span>
<br>**Mapping:** None 
<br>**Definition:** MaxFiles = ChirpCMSSWMaxFiles 
<br>**Description:** Please see if clause! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MaxHosts </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MaxRuntime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MaxWallTimeMins </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MaxWallTimeMinsRun </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MegaEvents </span>
<br>**Mapping:** None 
<br>**Definition:** MegaEvents = ChirpCMSSWEvents / 1e6 
<br>**Description:** The number of events processed by the job, in millions. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MemoryMB </span>
<br>**Mapping:** None 
<br>**Definition:** MemoryMB = (ResidentSetSize_RAW OR 0) / 1024.0 
<br>**Description:** The amount of RAM used by the job. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MemoryProvisioned </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MemoryUsage </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MinCores </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** MinHosts </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NiceUser </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NumGlobusSubmitsNumJobMatches </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NumJobCompletions </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NumJobMatches </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NumJobReconnects </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NumJobStarts </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NumPids </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NumRestarts </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NumShadowExceptions </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NumShadowStarts </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** NumSystemHolds </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OVERFLOW_CHECK </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OVERFLOW_IT </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OVERFLOW_UK </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OVERFLOW_US </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OnExitHoldReason </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OriginalCpus </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OriginalMaxWallTimeMins </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OriginalMemory </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OriginalReqs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Original_DESIRED_Sites </span>
<br>**Mapping:** None 
<br>**Definition:** Original_DESIRED_Sites = make_list_from_string_field(ad, ExtDESIRED_Sites) 
<br>**Description:** Please see the function! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OutputDestination </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OutputFiles </span>
<br>**Mapping:** None 
<br>**Definition:** OutputFiles = (<br>            len((CRAB_AdditionalOutputFiles OR []))<br>            + len((CRAB_TFileOutputFiles OR []))<br>            + len((CRAB_EDMOutputFiles OR []))<br>            + (CRAB_SaveLogsFlag OR 0)<br>        ) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OutputGB </span>
<br>**Mapping:** None 
<br>**Definition:** OutputGB = ChirpCMSSWWriteBytes / 1e9 
<br>**Description:** Amount of output written by the CMSSW process, in gigabytes. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** OverflowType </span>
<br>**Mapping:** None 
<br>**Definition:** OverflowType = "FrontendOverflow", "IgnoreLocality", "Unified" 
<br>**Description:** Please see if clauses! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Owner </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** PeriodicRemoveReason </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** PilotRestLifeTimeMins </span>
<br>**Mapping:** None 
<br>**Definition:** if analysis AND JobStatus == 2 AND (LastMatchTime is not  None ):<br>    PilotRestLifeTimeMins = int((MATCH_GLIDEIN_ToDie None EnteredCurrentStatus) / 60)<br>else:<br>    PilotRestLifeTimeMins = -72 * 60 
<br>**Description:** analysis = (Type == "analysis" OR CMS_JobType == "Analysis") 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** PostJobPrio1 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** PostJobPrio2 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** PreJobPrio1 </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ProcId </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Processor </span>
<br>**Mapping:** None 
<br>**Definition:** Processor = str(ChirpCMSSWCPUModels) OR str(MachineAttrCPUModel0) 
<br>**Description:** The processor model (from /proc/cpuinfo) the job last ran on.<br>if ChirpCMSSWCPUModels exist, else if MachineAttrCPUModel0 exists 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ProjectName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ProportionalSetSizeKb </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ProvisionedResources </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** QDate </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** QondorRundir </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** QueueHrs </span>
<br>**Mapping:** None 
<br>**Definition:** QueueHrs = ((JobCurrentStartDate OR time.time()) None QDate) / 3600.0 
<br>**Description:** Number of hours the job spent in queue before last run. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** REQUEST_OS </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** REQUIRED_OS </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Rank </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ReadOpSegmentPercent </span>
<br>**Mapping:** None 
<br>**Definition:** ReadOpSegmentPercent = (ChirpCMSSWReadOps / float(ops) * 100) 
<br>**Description:** Percentage of read segments done via a single read operation. <br>If a job issues a single read and a readv of 9 segments, this value would be 10.<br>ops = ChirpCMSSWReadSegments + ChirpCMSSWReadOps<br>Please see handle_chirp_info function 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ReadOpsPercent </span>
<br>**Mapping:** None 
<br>**Definition:** ReadOpsPercent = ChirpCMSSWReadOps / float(ops) * 100 
<br>**Description:** Percentage of reads done via a single read operation (as opposed to a vectored readv operation). <br>If a job issues a single read and a readv of 9 segments, this value would be 50.<br>ops = ChirpCMSSWReadSegments + ChirpCMSSWReadOps<br>Please see handle_chirp_info function 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ReadTimeHrs </span>
<br>**Mapping:** None 
<br>**Definition:** ReadTimeHrs = ChirpCMSSWReadTimeMsecs / 3600000.0 
<br>**Description:** Amount of time CMSSW spent in its IO subsystem for reads in hours 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ReadTimeMins </span>
<br>**Mapping:** None 
<br>**Definition:** ReadTimeMins = ChirpCMSSWReadTimeMsecs / 60000.0 
<br>**Description:** Amount of time CMSSW spent in its IO subsystem for reads in minutes 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RecentBlockReadBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RecentBlockReadKbytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RecentBlockReads </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RecentBlockWriteBytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RecentBlockWriteKbytes </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RecentBlockWrites </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RecentStatsLifetime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RecentStatsLifetimeStarter </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RecordTime </span>
<br>**Mapping:** None 
<br>**Definition:** RecordTime = recordTime(ad) 
<br>**Description:** Please see function! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ReleaseReason </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RemoteHost </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RemotePool </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RemoteSlotID </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RemoteSysCpu </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RemoteUserCpu </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RemoteWallClockTime </span>
<br>**Mapping:** None 
<br>**Definition:** RemoteWallClockTime = int(now None EnteredCurrentStatus) 
<br>**Description:** now = time.time() 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Remote_JobUniverse </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RemoveReason </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RepackslotsProvisioned </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RequestCpus </span>
<br>**Mapping:** None 
<br>**Definition:** see explanation=> 
<br>**Description:** Number of cores utilized by the job. Logic:<br>if RequestCpus is not exist in ClassAd:<br>  (try sequentially)<br>  -- look at CreamAttributes (see regex)<br>  -- look at NordugridRSL (see regex)<br>  -- look at xcount<br>else RequestCpus=1 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RequestDisk </span>
<br>**Mapping:** RequestDisk_RAW 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RequestGPUs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RequestMemory </span>
<br>**Mapping:** RequestMemory_RAW 
<br>**Definition:** None 
<br>**Description:** Amount of memory requested by the job, in MB. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RequestRepackslots </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Requestioslots </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Requirements </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RequiresCVMFS </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ResidentSetSize </span>
<br>**Mapping:** ResidentSetSize_RAW 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RouteName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RouteType </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RoutedBy </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RoutedFromJobId </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** RoutedToJobId </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** SINGULARITY_JOB </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ScheddBday </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ScheddInterval </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ScheddName </span>
<br>**Mapping:** None 
<br>**Definition:** ScheddName = (GlobalJobId OR "UNKNOWN").split("#")[0] 
<br>**Description:** Name of HTCondor schedd where the job ran. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ScratchDirFileCount </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ShadowBday </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ShouldTransferFiles </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** SinceLastCMSSWUpdateHrs </span>
<br>**Mapping:** None 
<br>**Definition:** SinceLastCMSSWUpdateHrs = (max(RecordTime None ChirpCMSSWLastUpdate, 0) / 3600.0) 
<br>**Description:** Please see if clause! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** SingularityImage </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Site </span>
<br>**Mapping:** None 
<br>**Definition:** Site = (MATCH_EXP_JOB_GLIDEIN_CMSSite OR "UNKNOWN") 
<br>**Description:** CMS site where the job ran (for example, T2_CH_CSCS). <br>Since cms is true, no need to explain remaining elif clauses! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** SpoolOnEvict </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** StageInFinish </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** StageInStart </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** StageOutHrs </span>
<br>**Mapping:** None 
<br>**Definition:** StageOutHrs = SinceLastCMSSWUpdateHrs 
<br>**Description:** An estimate of the stageout time, in hours. Calculated from WallClockHr-CMSSWWallHrs. <br>The veracity of this attribute is not known. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** StartdIpAddr </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** StartdPrincipal </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** StarterIpAddr </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** StatsLifetime </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** StatsLifetimeStarter </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Status </span>
<br>**Mapping:** None 
<br>**Definition:** Status = status.get(JobStatus, "Unknown") 
<br>**Description:** State of the job; valid values include Completed, Running, Idle, or Held. <br>Note; due to some validation issues, non-experts should only look at <br>completed jobs. Please see status dict. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** StreamIn </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** SubmitFile </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** THETAJob </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TIER0_WF </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TaskType </span>
<br>**Mapping:** None 
<br>**Definition:** TaskType = (CMS_TaskType OR (guessTaskType(ad) if not analysis else CMS_JobType)) 
<br>**Description:** A more detailed task type classification, based on the CMSSW config. <br>Typically, analysis, DIGI, RECO, DIGI-RECO, GEN-SIM, or Cleanup. <br>Is set to the CMS_TaskType classad if exists, otherwise it is infered for production jobs.<br>analysis = (Type == "analysis" OR CMS_JobType == "Analysis")<br>Please see guessTaskTypeFunction! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TerminationPending </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Tier </span>
<br>**Mapping:** None 
<br>**Definition:** Site.split("_", 2)[0] 
<br>**Description:** Tier where the job ran (for example, T2). <br>Since cms is true, no need to explain remaining if/elif clauses! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TimePerEvent </span>
<br>**Mapping:** None 
<br>**Definition:** TimePerEvent = 1.0 / EventRate 
<br>**Description:** The inverse of EventRate 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ToE.How </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ToE.HowCode </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ToE.When </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** ToE.Who </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TotalSubmitProcs </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TotalSuspensions </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferErr </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferExecutable </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferInFinished </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferInQueued </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferInStarted </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferInputSizeMB </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferOut </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferOutFinished </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferOutQueued </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferOutStarted </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferOutputRemaps </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferQueued </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferringInput </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** TransferringOutput </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Type </span>
<br>**Mapping:** None 
<br>**Definition:** Type = (CMS_Type OR "unknown").lower() 
<br>**Description:** The kind of job run; analysis or production. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Universe </span>
<br>**Mapping:** None 
<br>**Definition:** Universe = universe.get(JobUniverse OR "Unknown") 
<br>**Description:** Please see universe dict 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** User </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** VO </span>
<br>**Mapping:** None 
<br>**Definition:** VO = str(x509UserProxyVOName) 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WMAgent_AgentName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WMAgent_JobID </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WMAgent_RequestName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** The WMAgent request name (for production jobs only); <br>example; pdmvserv_task_HIG-RunIISpring16DR80-01026__v1_T_160530_083522_4482. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WMAgent_SubTaskName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** The WMAgent subtask name (for production jobs only), example; <br>pdmvserv_task_HIG-RunIISpring16DR80-01026__v1_T_160530_083522_4482/HIG-RunIISpring16DR80-01026_0 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WMAgent_TaskType </span>
<br>**Mapping:** None 
<br>**Definition:** WMAgent_TaskType = (WMAgent_SubTaskName OR "/UNKNOWN").rsplit("/", 1)[-1] 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WMCore_ResizeJob </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WallClockCheckpoint </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WallClockHr </span>
<br>**Mapping:** None 
<br>**Definition:** WallClockHr = (RemoteWallClockTime OR 0) / 3600.0 
<br>**Description:** Number of hours the job spent running. This is invariant of the number of <br>cores; most users will prefer CoreHr instead. 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WantGracefulRemoval </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WantIOProxy </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WantJobRouter </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WhenToTransferOutput </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** Workflow </span>
<br>**Mapping:** None 
<br>**Definition:** Workflow = guessWorkflow(ad, analysis) 
<br>**Description:** Human-readable workflow name. Example; HIG-RunIISpring16DR80-01026_0<br>analysis = (Type == "analysis" OR CMS_JobType == "Analysis")<br>Please see guessWorkflow function! 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WriteTimeHrs </span>
<br>**Mapping:** None 
<br>**Definition:** WriteTimeHrs = ChirpCMSSWWriteTimeMsecs / 3600000.0 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** WriteTimeMins </span>
<br>**Mapping:** None 
<br>**Definition:** WriteTimeMins = ChirpCMSSWWriteTimeMsecs / 60000.0 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** X509UserProxy </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** x509UserProxyEmail </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** x509UserProxyFQAN </span>
<br>**Mapping:** None 
<br>**Definition:** x509UserProxyFQAN = str(x509UserProxyFQAN).split(",") 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** x509UserProxyFirstFQAN </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** x509UserProxyVOName </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** None 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
<span style="color:#4169E1">**Name:** x509userproxysubject </span>
<br>**Mapping:** None 
<br>**Definition:** None 
<br>**Description:** The DN of the grid certificate associated with the job; for CMS jobs, <br>this is not the best attribute to use to identify a user (prefer CRAB_UserHN) 
<br>**Reference:** [ref1](https://github.com/dmwm/cms-htcondor-es/blob/master/src/htcondor_es/convert_to_json.py) 
<br>**Origin:** Spider 
<br>**Tool:** Spider 
<br>**Contact:** CMS Monitoring, DMWM group 
---
