# HTCondor job monitoring

The [spider](../Operators/spider.md) is a set of scripts which consumes CMS HTCondor ClassAds (Job processing docs), converts them to JSON documents and feeds them via [CMSMonitoring/StompAMQ](https://github.com/dmwm/CMSMonitoring/blob/master/src/python/CMSMonitoring/StompAMQ.py) module to the CERN MONIT infrastructure.

  - Please see the **[documentation](./cms-htcondor-es.md)** of ALL fields in Raw classAds in ES.
  - Spider code on [github](https://github.com/dmwm/cms-htcondor-es/blob/master/README.md)
  - Instructions for [operators](../Operators/spider.md)
  - The main job monitoring attributes and their implementation in ES are tracked [here](https://docs.google.com/spreadsheets/d/1x73SxboZqNvpYKHmaJ9PioT3itpw4qQrTjUecnKLTi4/edit?usp=sharing). A few relevant fields from the job monitoring dashboards:

     - __Tier__, __Site__, __Country__: Tier, name, and country of the site running the job. Can be unknown for jobs that are not yet acquired by any site.
     - __Type__: Either 'analysis' or 'production'
     - __CMSPrimaryDataTier__: Data tier, as determined from splitting the full dataset name (as stored in the `DESIRED_CMSDataset` field). 'Unknown' if not found.
     - __CMS_JobType__: Job type for production jobs, i.e. 'Processing', 'Production', 'Merge', 'LogCollect', 'Cleanup', etc. 'Analysis' for analysis jobs, and 'Unknown' if not specified.
     - __ScheddName__: Name of the condor schedd handling the job.(Currently only available in the [12m dashboard](https://monit-grafana.cern.ch/d/o3dI49GMz/cms-job-monitoring-12m?orgId=11&var-group_by=ScheddName))
     - __TaskType__: Determined from `WMAgent_SubTaskName` field. 
     - __ExitCode__: Exit code of the cmsRun application itself. (In the range -1–512). See also [here](https://twiki.cern.ch/twiki/bin/view/CMSPublic/JobExitCodes)
     - __CRAB_AsyncDest__: Name of the destination site, for analysis jobs only.
     - __InputData__: Either 'Offsite' or 'Onsite'.
     - __CMSSWVersion__, __CMSSWReleaseSeries__, __CMSSWMajorVersion__: Full version name (e.g. '9_4_3_patch2'), minor version name (e.g. '9_4_X'), and major version name (e.g. '9_X_X')
     - __Chirp_CRAB3_Job_ExitCode__: Job exitcode from CRAB. See also [here](https://twiki.cern.ch/twiki/bin/view/CMSPublic/JobExitCodes)


## Condor Jobs Data Flow

[![Condor Job Data](https://www.dropbox.com/s/pjzludtd32ie6a2/Condor%20Jobs%20Data%20Monit.png?raw=1)](https://www.dropbox.com/s/2oueo3i9hqnzlk2/Condor%20Jobs%20Data%20Monit.pdf?dl=0)

Click on the image to download the pdf version.

