# HDFS

Data on HDFS are tipically accessed using a Spark cluster. First of all you need to get authorised to access a Spark-based cluster at CERN (analytix is the general purpose cluster for spark-based workflows):

- HDFS from lxplus7: use method described [here](https://cern.service-now.com/service-portal/article.do?n=KB0004426)
- How to connect to a CERN Spark cluster from [lxplus7](https://cern.service-now.com/service-portal/article.do?n=KB0004426) or [SWAN](https://github.com/swan-cern/help/blob/master/spark/clusters.md) 

To access data on HDFS check our [tutorials](../training/README.md) (covering CMSSpark, Swan, Rumble).

