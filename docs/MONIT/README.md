# The MONIT infrastructure

## Data injection

- Data can be injected into MONIT through [AMQ](injection.md) or HTTP (for data producers inside the CERN network),
- [CMSWEB logs](sources.md#cmsweb-logs) are injected with [Logstash](../logstash/README.md).

## Data sources and visualisation/access

The CMS MONIT data [sources](sources.md) are:

- [ElasticSearch](https://www.tutorialspoint.com/elasticsearch/index.htm), can be accessed trough [Kibana](Kibana.md), [Grafana](Grafana.md), [API token](Grafana.md#grafana-token).
- [InfluxDB](https://www.influxdata.com/products/influxdb-overview/), can be accessed through [Grafana](Grafana.md), [API token](Grafana.md#grafana-token).
- [HDFS](https://www.geeksforgeeks.org/hdfs-commands/), can be accessed via [spark jobs and SWAN](HDFS.md) service. 


| Source        | Type of data  | Retention  |  Access |
| ------------- |:-------------:| ----------:|------------:|
| ES cms        | raw           | 1.5 y      | Kibana/Grafana |
| ES MONIT      | raw           | 30-40 days | Kibana/Grafana |
| ES MONIT      | aggregated    | infinite | Kibana/Grafana |
| InfluxDB      | aggregated    | infinite   |   Grafana |
| HDFS          | raw           | infinite   |   SWAN/analytix |
| HDFS          | logs          | 13 months   |   SWAN/analytix |

A list of [repositories](code.md) from data providers on how they are produced (and what they contain).

## [HTCondor job monitoring](condor.md)

## [Tutorials](../training/README.md)

## MONIT Architecture

![MONIT architecture](MONIT.png)
