### Data aggregation
This document contains instructions how to perform data aggregation
with CERN MONIT and local infrastructures.

#### Data aggregation between Prometheus and CERN MONIT
It is possible to aggregate Prometheus records for long term storage in
CERN MONIT. For that you need to properly configure Prometheus configuration.
Here we provide details of such changes.

1. Locate your Prometheus configuration and add the following rule file entry
   (you'll need to adjust it to provide your path and rule file name, in this
   case our rule file is located in `/etc/prometheus/srv.rules`)

```
rule_files:
  - "/etc/prometheus/srv.rules"
```

2. Create your service rule file with the following content (here we define
`dbs` group name with specific sum average record:
```
groups:
- name: dbs
  rules:
  - record: sum_avg_dbs_global_exporter_proc_cpu
    expr: sum(avg_over_time(dbs_global_exporter_proc_cpu[1h]))
    labels:
      aggregate: 1h
      monit_forward: "true"
```
The record should contain at least two labels. The `aggregate` interval
and `monit_forward`.

Once such rules in place your records will appear in CERN MONIT InfluxDB and
be stored for up to 3 years. You may build appropriate dashboard from them
using `monit-central-prometheus` data-source.
