# The k8s clusters

* The CMS monitoring k8s infrastructure is described [here](../infrastructure/k8s.md)
* Basic concepts and end-to-end deployment of a basic application to a k8s cluster in this [tutorial](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/docs/end-to-end.md).
* [cmsweb-k8s-admin](https://gitlab.cern.ch:8443/cmsweb-k8s-admin/k8s_admin_config) contains configuration files for our clusters **Please note**: this page is only visible to members of CMS HTTP group. Therefore, if you need to access configuration files please contact [CMSWEB operator](mailto:cms-service-webtools@cern.ch).
* [monitoring secrets](https://gitlab.cern.ch/cms-monitoring/secrets) contains
  all necessary secret files used in k8s monitoring clusters
* Since we deploy cluster on k8s infrastructure their deployment are alike. Here
we only refer to a single deployment of monitoring cluster.

## Setup the CMS monitoring cluster

The full instructions how to setup the cluster can be found 
[here](https://github.com/dmwm/CMSKubernetes/tree/master/kubernetes/monitoring/README.md).

## Cluster maintenance

The maintenance of the cluster is quite trivial. 
The operator is expected to follow these steps:

1) login to `lxplus-cloud`

2) clone the CMSKubernetes repository
```
git clone git@github.com:dmwm/CMSKubernetes.git
cd CMSKubernetes/kubernetes/monitoring
```
3) clone the CMS Monitoring secrets repository
```
git clone https://:@gitlab.cern.ch:8443/cms-monitoring/secrets.git
```
4) Setup the environment (cluster config file comes from secrets repository)
```
export OS_PROJECT_NAME="CMS Web"
export KUBECONFIG=/<path>/config
```
5) perform various k8s commands, e.g. check node/pods health
```
# check node status
kubectl get node
kubectl top node
# check pod status
kubectl get pods
kubectl top pods
# use deploy.sh script to get status of the cluster
./deploy.sh status
# explore logs of certain pods
kubectl logs <pod-name>
```
6) If a node gets stuck:
```
openstack server reboot --hard monitoring-cluster-ysvr3hqxecfu-master-0 
```

## CMSWeb k8s cluster

To learn about cmsweb k8s architecture please read the
[architecture](https://cms-http-group.docs.cern.ch/k8s_cluster/architecture/) document.

To deploy cmsweb and monitoring services on kubernetes please follow the steps
outlined in [cms-http-group.docs.cern.ch](https://cms-http-group.docs.cern.ch/)
documentation.

For more information about specific steps please follow steps outlined
in [CMSKubernetes](https://github.com/dmwm/CMSKubernetes/tree/master/kubernetes/cmsweb/docs)
repository, e.g.

- [cluster creation](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/docs/cluster.md)
- [general deployment](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/docs/deployment.md)
- [cmsweb deployment](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/docs/cmsweb-deployment.md)
- [nginx](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/docs/nginx.md)
- [autoscaling](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/docs/autoscaling.md)
- [storage](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/docs/storage.md) (this step is optional)
- [hadoop](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/docs/hadoop.md) (this step is optional)
- [troubleshooting](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/docs/troubleshooting.md)
- [references](https://github.com/dmwm/CMSKubernetes/blob/master/kubernetes/cmsweb/docs/references.md)
