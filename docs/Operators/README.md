# Documentation for CMS operators

## General

* How to manage this [documentation](../infrastructure/build.md)
* [Dashboards](dashboard.md) mantainance
* [Slack monitoring group](https://cmsmonitoringgroup.slack.com/) to access alerts routed by AlertManager service

## Virtual machines and services

 - Service [accounts](accounts.md) 
 - [acronjobs](../infrastructure/acronjobs.md) running on lxplus
 - List of [nodes](nodes.md) used for various services
     - [Spider](spider.md)
     - [WMA and WMArchive](wma.md) monitoring
     - [Data Popularity](popularity.md)
     - [CMSSW](cmssw.md) popularity service

## k8s infrastructure and services

* [k8s](k8s.md) monitoring clusters
* List of [services](../infrastructure/services.md)
* [Alerts](../AlertManagement/README.md)
* [Exporters](../Exporters/README.md) 
* [sqoop jobs](../MONIT/sources.md#additional-data-sources-from-sqoop-jobs)
