# CMSSW popularity service
We run CMSSW popularity service on dedicated *vocms0200* node.
It is run to support backward compatibilty link used in all GRID nodes.
To maintain the service please use the following set of instructions:

```
# login to vocms0200 node
ssh vocms0200

# switch to cmspopdb account
sudo -i -u cmspopdb /bin/bash

# locate service
cd /opt/udp

# To manage all services please use /opt/udp/udp-collector.sh script
$ udp-collector.sh start|stop|restart|status

# To manage individual tasks please use these instructions:

# start UDP collector service with:
$ export PATH=/opt/udp:$PATH
$ nohup ./udp_server_monitor 2>&1 1>& udp_server.log < /dev/null &

# start node exporter
$ export PATH=$PWD:$PATH
$ nohup ./node_exporter 2>&1 1>& node_exporter.log < /dev/null &

# start process exporter:
$ export PATH=$PWD:$PATH
$ nohup ./process_monitor.sh "udp_server -config udp_server.json" udp_collector ":9101" 5 2>&1 1>& process_monitor.log < /dev/null &
```
