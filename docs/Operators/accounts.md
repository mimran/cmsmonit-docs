# CMS Monitoring Accounts

We use different accounts for different purposes:

- **cmssqoop** is used to run HDFS related tasks on k8s clusters, e.g. [sqoop jobs](../MONIT/sources.md#additional-data-sources-from-sqoop-jobs), [hdfs exporter](../Exporters/README.md), etc. It is often used to obtain a valid kerberos ticket:
    - [kerberos ticket](https://github.com/dmwm/CMSKubernetes/blob/master/docker/sqoop/run.sh#L3-L12)
  for sqoop run script
    - [kerberos script](https://github.com/dmwm/CMSKubernetes/blob/master/docker/kerberos/kerberos.sh)
  for kerberos cron jobs
  These scripts rely on keytab file supplied via k8s secrets. The keytab files can be obtained in appropriate area of our restricted  [secrets](https://gitlab.cern.ch:8443/cms-monitoring/secrets) gitlab repository.
- **cmsmonit** is used to 
    - maintain our documentation and manage the `cmsmonit-docs.web.cern.ch` Openshift [web site](https://openshift.cern.ch/console/project/cmsmonit-docs/)
    - run cron jobs on lxplus7 (should be eventually migrated to k8s), particularly for [EOS data popularity](popularity.md#eos-popularity), [event count plots](popularity.md#event-count-plots), and [CMSSW data popularity](cmssw.md)
- **cmspopdb** is used to run various cronjobs on vocms092 for [popularity](popularity.md#dataset-popularity-based-on-dbs-phedex-and-condor-dataset) based on DBS, PhEDEx and condor datasets
- **cmsjobmon** is used to run the [spider](spider.md) service
