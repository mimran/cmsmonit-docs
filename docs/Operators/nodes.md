# CMS Monitoring nodes

Here we list all CMS Monitoring nodes:

 - **vocms092** runs the following list of [services](popularity.md):
    - phedex PhEDEx snapshots
    - dbs events aggregation
    - DBS DB dump
    - Phedex block replicas snapshots
    - Condor snapshots
 - **vocms0200** runs the [CMSSW popularity](cmssw.md) service (UDP based)
 - **vocms0240** runs the [spider](spider.md) service
- **vocms055** runs [logstash](../MONIT/sources.md#cmsweb-logs) service for CMSWEB VM logs
- **vocms0181** and **vocms0182** run the [WMArchive](wma.md) service (obsolete since it is
  ported to k8s)
