# CMS monitoring [project](https://cms-monitoring.cern.ch/)


## CERN IT MONIT [infrastructure](MONIT/README.md)

   - [Grafana](MONIT/Grafana.md),
   - [Kibana](MONIT/Kibana.md) and ElasticSearch,
   - [AMQ](MONIT/README.md#data-injection),
   - [HDFS](MONIT/HDFS.md) and Spark,
   - CMS data [sources](MONIT/sources.md) and [code](MONIT/code.md),
   - [HTCondor job monitoring](MONIT/condor.md).

## CMS monitoring [infrastructure](infrastructure/README.md)

   - [Prometheus](infrastructure/README.md#cms-prometheus-services),
   - [AlertManager](infrastructure/alertmanager.md),
   - [VictoriaMetrics](infrastructure/vm.md),
   - [NATS](NATS/nats.md),
   - [CLI tools](infrastructure/README#cms-monitoring-cli-tools).

## Documentation for monitoring [operators](Operators/README.md)

## [Tutorials](training/README.md)

## References

* C Ariza-Porras, V Kuznetsov, F Legger, *The CMS monitoring infrastructure and applications*, Computing and Software for Big Science volume 5, Article number: 5 (2021) [link](https://doi.org/10.1007/s41781-020-00051-x).

## Contacts
   - Please open a [JIRA ticket](https://its.cern.ch/jira/browse/CMSMONIT) for specific requests,
   - or reach us at [cms-comp-monit](mailto:cms-comp-monit@cern.ch) for general discussions.

![monitoring architecture](MONIT4CHEP.png)
