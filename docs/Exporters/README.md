# Prometheus Exporters

We rely on [Prometheus exporters](https://prometheus.io/docs/instrumenting/exporters/) to collect appropriate information from nodes, databases, applications and services:

- [node exporter](https://github.com/prometheus/node_exporter/blob/master/README.md) is used to
  monitor hardware metrics of the node
- [MongoDB exporter](https://github.com/dcu/mongodb_exporter/blob/master/README.md) to monitor
  MongoDB database
- [HDFS exporter](https://github.com/dmwm/CMSMonitoring/blob/master/src/go/MONIT/hdfs_exporter.go)
  to monitor usage of any HDFS area
- [cmsweb exporters](https://github.com/dmwm/cmsweb-exporters/blob/master/README.md) a series of
  auxilary exporters to monitor CMSWEB services
  - [process exporter](https://github.com/dmwm/cmsweb-exporters/blob/master/process_exporter.go)
    monitor given process ID, see complementary
    [process_monitor.sh](https://github.com/dmwm/cmsweb-exporters/blob/master/process_monitor.sh)
    script
  - [CherryPy exporter](https://github.com/dmwm/cmsweb-exporters/blob/master/cpy_exporter.go)
    to monitor CherryPy Python based HTTP service
  - [cmsweb ping exporter](https://github.com/dmwm/cmsweb-exporters/blob/master/cmsweb-ping.go)
    to ping given HTTP service on CMSWEB cluster
  - [eos exporter](https://github.com/dmwm/cmsweb-exporters/blob/master/eos_exporter.go)
    to monitor EOS area usage
  - [http exporter](https://github.com/dmwm/cmsweb-exporters/blob/master/http_exporter.go)
    to monitor HTTP service

## How to write your own exporter

Please refer to the official Prometheus
[manual](https://prometheus.io/docs/instrumenting/writing_exporters/) on how to
write exporter, or you can use any exporter from [cmsweb-exporters](https://github.com/dmwm/cmsweb-exporters)
area to see its structure.

## Deployment procedure

Since all our exporters are written in Go language it is trivial to deploy it to any Linux node. For that you only need to compile the exporter code in the following way
```
go build <exporter_file>.go
```
The exporter executable can be copied to Linux node and run without setting up
any environment.

On kubernetes we have several manifest files referring to our exportrers.
Please see corresponding
[monitoring/services](https://github.com/dmwm/CMSKubernetes/tree/master/kubernetes/monitoring/services)
area and look-up appropriate exporter yaml file.
