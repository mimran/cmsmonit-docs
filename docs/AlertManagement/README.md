# Alerts

## Overview

The growth of distributed services introduces a challenge to properly monitor their status and reduce operational costs. In CMS experiment at CERN we deal with distributed computing infrastructure which includes central services for authentication, workload management, data management, databases, etc. To properly operate and maintain this infrastructure we rely on various open-source monitoring tools, including ElasticSearch, Kafka, Grafana stack (used by central CERN MONIT infrastructure), Prometheus, AlertManager, VictoriaMetrics software components (used by the experiment), as well as custom solutions like GGUS WLCG or ServiceNow ticketing systems.

On daily basis these CMS computing infrastructure may produce significant amount of information about various anomalies, intermittent problems, outages as well as undergo scheduled maintenance. Therefore the amount of alert notifications and tickets our operational teams should handle is very large. We’re working towards, an Operational Intelligent System aiming to detect, analyse, and predict anomalies of the computing environment, to suggest possible actions, and ultimately automate operation procedures. An important component of this system should be an intelligent Alert Management System.

The system should collect anomalies, notifications, etc., from ES, InfluxDB, Prometheus data-sources, GGUS system and be able to aggregate, and create meaningful alerts to address various computing infrastructure failures. For instance, if certain component of global distributed system experiences a problem in terms of its storage the Alert Management system should notify via appropriate channels a set of instructions on how to fix and handle this situation. Before sending the alert it should check if this is a real anomaly or part of on-going outage, or schedule maintenance.

For detailed information please visit the [blog](https://indrarahul.codes/2020/07/24/google-summer-of-code.html).

## Architectural Diagram
![architecture](images/arch_diag.png)

## Installation

[How](installation.md) to setup this project.

## SSB Alerting Service

[CLI tools](../infrastructure/README.md#cms-monitoring-cli-tools) included in this service are:

- [monit](https://github.com/dmwm/CMSMonitoring/blob/master/src/go/MONIT/monit.go)
- [ssb_alerting](https://github.com/dmwm/CMSMonitoring/blob/master/src/go/MONIT/ssb_alerting.go)
- [ssb_alert.sh](https://github.com/dmwm/CMSMonitoring/blob/master/scripts/ssb_alert.sh)
- [ssb_alert_manage](https://github.com/dmwm/CMSMonitoring/blob/master/scripts/ssb_alert_manage)

### monit

The program which is responsible for the SSB queries from InfluxDB. A query can be crafted according to our needs. 

An example of such a query :-

```monit -query="select * from outages where time > now() - 2h" -dbname=monit_production_ssb_otgs -token=$token -dbid=9474```

where we are fetching outages record of last 2 hrs.

### ssb_alerting

monit queries from InfluxDB and dumps the result on stdout which can be dumped on the disk. ssb_alerting fetches dumped SSB JSON Data and converts it to a JSON format which AlertManager's API understands. There could be one issue where some of SSB alerts have no EndTime, i.e. they have open ending. So to solve this issue on each iteration we fetch alerts from AlertManager and check if some alerts having open ending are resolved or not from the maintained HashMap which has information about current alerts. If they are resolved they are pushed with current time as EndTime i.e. we delete those alerts from AlertManager.

```
Usage of ssb_alerting:
  -input string
    	input filename
  -url string
    	alertmanager URL
  -verbose int
    	verbosity level
```

The dataflow and logic behind ssb_alerting tool can be visualized in the diagram below.
![Alt text](images/alerting.jpg)

### ssb_alert.sh

A simple bash script which makes the above process automated on configurable time interval value.

```
Script for fetching CERN SSB info and injecting it into MONIT (AlertManager)
Usage: ssb_alerts.sh <query> <token> [cmsmon_url] [interval] [verbose]

  <query>       CMS Monit ES/InfluxDB Query
  <token>       User's Token
   
Options:
  cmsmon_url    CMS Monitoring URL                              (default: https://cms-monitoring.cern.ch)
  interval      Time interval for Alerts ingestion & injection  (default: 1)
  verbose       Verbosity level                                 (default: 0)
```

### ssb_alert_manage
A Linux Daemon for our ssb_alerting mechanism which runs our alerting service in the background. It has four open commands like start, stop, status, help to control the ssb alerting service without much of hassle.
Few Environment Variables are required to be set ( PS. some of them have default values.) which makes the whole service easily configurable. 

```
Environments:
  QUERY       :   CMS Monit ES/InfluxDB Query              
  TOKEN       :   User's Token
  CMSMON_URL  :   CMS Monitoring URL                              default - https://cms-monitoring.cern.ch"
  INTERVAL    :   Time interval for Alerts ingestion & injection  default - 1
  VERBOSE     :   Verbosity level                                 default - 0
```

## GGUS Alerting Service

[CLI tools](../infrastructure/README.md#cms-monitoring-cli-tools) included in this service are:

- [ggus_parser](https://github.com/dmwm/CMSMonitoring/blob/master/src/go/MONIT/ggus_parser.go)
- [ggus_alerting](https://github.com/dmwm/CMSMonitoring/blob/master/src/go/MONIT/ggus_alerting.go)
- [ggus_alert.sh](https://github.com/dmwm/CMSMonitoring/blob/master/scripts/ggus_alert.sh)
- [ggus_alert_manage](https://github.com/dmwm/CMSMonitoring/blob/master/scripts/ggus_alert_manage)

### ggus_parser

The program which uses HTTP request to fetch GGUS Tickets using CERN Grid Certificate in XML/CSV Format which is then parsed and converted into usable JSON Format and dumped to the disk.

### ggus_alerting

ggus_alerting fetches dumped SSB JSON Data and converts it to a JSON format which AlertManager's API understands. There could be one issue where some of GGUS alerts have no EndTime, i.e. they have open ending. So to solve this issue on each iteration we fetch alerts from AlertManager and check if some alerts having open ending are resolved or not from the maintained HashMap which has information about current alerts. If they are resolved they are pushed with current time as EndTime i.e. we delete those alerts from AlertManager.

```
Usage of ggus_alerting:
  -input string
    	input filename
  -url string
    	alertmanager URL
  -verbose int
    	verbosity level
  -vo string
    	Required VO attribute in GGUS Ticket (default "cms")
```

The dataflow and logic behind ggus_alerting tool can be well visualized in the below diagram. 
![diagram](images/alerting.jpg)

### ggus_alert.sh

A simple bash script which makes the above process automated on configurable time interval value.
```
Script for fetching GGUS Tickets and injecting them into MONIT (AlertManager)
Usage: ggus_alerts.sh [ggus_format] [cmsmon_url] [interval] [vo] [timeout] [verbose]

Options:
  ggus_format   GGUS Query Format ("csv" or "xml")              (default: "csv")
  cmsmon_url    CMS Monitoring URL                              (default: https://cms-monitoring.cern.ch)
  interval      Time interval for Alerts ingestion & injection  (default: 1)
  vo            Required VO attribute                           (default: "cms")
  timeout       HTTP client timeout operation (GGUS Parser)     (default:0 - zero means no timeout)
  verbose       Verbosity level                                 (default: 0)
```

### ggus_alert_manage

A Linux Daemon for our ssb_alerting mechanism which runs our alerting service in the background. It has four open commands like start, stop, status, help to control the ssb alerting service without much of hassle.
Few Environment Variables are required to be set ( PS. some of them have default values.) which makes the whole service easily configurable. 

```
Environments:
  GGUS_FORMAT :   GGUS Query Format("csv" or "xml")               default - "csv"
  CMSMON_URL  :   CMS Monitoring URL                              default - https://cms-monitoring.cern.ch"
  INTERVAL    :   Time interval for Alerts ingestion & injection  default - 1
  VO          :   Required VO attribute                           default - "cms"
  TIMEOUT     :   HTTP client timeout operation (GGUS Parser)     default - 0 (zero means no timeout)
  VERBOSE     :   Verbosity level                                 default - 0
```

## Karma Dashboard

"Alertmanager UI is useful for browsing alerts and managing silences, but it’s lacking as a dashboard tool - karma aims to fill this gap."     
-Karma Developer

You also get the URL for alerts which land you to the original ticketing platform. It gives a nice and intuitive view. Multi grid option, collapsing alerts, viewing silences are few nice features of Karma. Below screenshots show the karma dashboard with alerts from both of the services developed. 

![Alt text](images/karma1.png)

![Alt text](images/karma2.png)

## Slack
Slack has defined channels for particular service alerts. Users are notified about fired alerts which are drived by AlertManager bots. 

GGUS Alerts in Slack.

![Alt text](images/slack1.png)

SSB Alerts in Slack.

![Alt text](images/slack2.png)

## Alert CLI Tool

Nice and clean CLI interface for getting alerts, their details on terminal either in tabular form or JSON format. Convenient option for operators who prefer command line tools. Comes with several options such as :-
 - service, severity, tag - Filters
 - sort - Sorting
 - name - For detailed information of an alert
 - json - information in JSON format

```
Usage: alert [options]
  -generateConfig
    	Flag for generating default config
  -json
    	Output in JSON format
  -name string
    	Alert Name
  -service string
    	Service Name
  -severity string
    	Severity Level of alerts
  -sort string
    	Sort data on a specific Label
  -tag string
    	Tag for alerts
  -token string
    	Authentication token to use
  -verbose int
    	verbosity level, can be overwritten in config

Environments:
	CONFIG_PATH:	 Config to use, default (/home/z3r0/.alertconfig.json)

Examples:
	Get all alerts:
	    alert

	Get all alerts in JSON format:
	    alert -json

	Get all alerts with filters (-json flag will output in JSON format if required):
	Available filters:
	service	Ex GGUS,SSB,dbs,etc.
	severity	Ex info,medium,high,urgent,etc.
	tag		Ex cmsweb,cms,monitoring,etc.

	Get all alerts of specific service/severity/tag/name. Ex GGUS/high/cms/ssb-OTG0058113:
	    alert -service=GGUS
	    alert -severity=high
	    alert -tag=cms
	    alert -name=ssb-OTG0058113

	Get all alerts based on multi filters. Ex service=GGUS, severity=high:
	    alert -service=GGUS -severity=high

	Sort alerts based on labels. The -sort flag on top of above queries will give sorted alerts.:
	Available labels:
	severity	Severity Level
	starts		Starting time of alerts
	ends		Ending time of alerts
	duration	Lifetime of alerts

	Get all alerts of service=GGUS, severity=high sorted on alert's duration:
	    alert -service=GGUS -severity=high -sort=duration

	Get all alerts of service=GGUS sorted on severity level:
	    alert -service=GGUS -sort=severity
```

Below is a screenshot of such queries using the tool.

#### Screenshots
Alert CLI Tool printing all alerts in the alertmanager of type SSB services which are sorted over duration of each alert.

![Alt text](images/alert_tool1.png)

Alert CLI Tool printing all alerts in the alertmanager whose severity values are "high".

![Alt text](images/alert_tool2.png)

Alert CLI Tool printing a specific alert in details.

![Alt text](images/alert_tool3.png)


Alert CLI Tool printing a specific alert in details in json format.

![Alt text](images/alert_tool4.png)

## [Intelligence Module](../intelligence/README.md)


