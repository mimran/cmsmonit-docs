# The intelligence module

It is a data pipeline. Each components are independent of each other. One component receives the data, adds its logic and forwards the processed data to other component.

Below is the Intelligence module architecture diagram.

![Alt text](../AlertManagement/images/int_mod.png)

What does it do?

- assigns proper severity levels to SSB/GGUS alerts which helps operators to understand the criticality of the infrastructure. Ex. If Number of Alerts with severity=”urgent” > some threshold, then the infrastructure is in critical situation.
- annotates Grafana Dashboards when Network or Database interventions.
- predicts type of alerts and groups similar alerts with the help of Machine Learning.
- adds applicable tutorial/instructions URL/doc to alert, on following which an operator can solve the issue quickly.

## The Logic behind the intelligence module

1. The main function will run all pipeline's logic.
2. pipeline starts with fetching alerts,
3. each alert is processed to check if it has already been processed and silenced. If they are then we ignore that alert, otherwise, we pass it to the next pipeline component. (here we require the SilencedMap that stores all those alerts which are in Silence Mode, indicates they are processed and we should not repeat the intelligence process again for them ).
4. alerts pass through keyword matching were keywords are matched and accordingly severity level is assigned.
5. alerts pass through ML Box with no logic as of now.
6. the processed alert is pushed to AM and,
7. the old alert with default severity is silenced
8. resolved alerts that are silenced (i.e. when GGUS alerts are resolved) are deleted. This step for those alerts which have open ending. We wait for them till they are resolved.

## Testing

We use counters to verify if the intelligence module testing was successful or not. If there is mismatch in the counters we declare failure. We also check if the module successfully annotated the dashboards or not. To pass the test, the module shouldn't make random values in counters and it should annotate the dashboards.

- When we are fetching the alerts at step 2, we will count the number of alerts in the AM (i.e. before intelligence module does its stuff)
- Then when we go to preprocessing step 3, we will count all active, expired silences when we update our SilenceMap.
- When we create new Silences we keep adding 1 to the Active Silence counter which we modified at step 3.
- When we delete a resolved alert's Silence we keep adding 1 to the Expired Silence counter which we modified at step 3.

At the end of pipeline, we will end up having following counters:-

- No Of Active Silences
- No Of Expired Silences
- No of Pending Silences

We verify if everything went well at the end of pipeline. We will again fetch Alerts/Silences from AM and count them and check if they matches with the counters above. If they match, check for the "AnnotateTestStatus" value, if the module has annnotated the dashboards, it will be true and thus Test will be succesfull or if any one of them fails, Testing fails.

**MANUAL CHECK**
Also look on number of alerts counter. Although we can't say before and after running of intelligence module this counter should be equal because any time new alert can come in and we may be in the middle of run of our intelligence module. But we do know the number of alerts after running the intelligence module should not go high (exponential) as limited number of alerts are created at GGUS/SSB. So if you see unexpectedly high increase in number of alerts after running the intelligence module, consider the testing failed. 

We also are providing solution for silencing false alerts which are created due to maintenance alert. The maintenance alert has a field of instances which is a list of all those instance which are going to get affected due to this ongoing maintenance. So our [intelligence program](https://github.com/dmwm/CMSMonitoring/blob/master/src/go/MONIT/intelligence.go), goes to Alertmanager fetches all alerts and filters them based on the searchingLabel (here instance) and silence them for time the maintenance is going. Once the maintenance ends the if alerts are still alive they come back to the AlertManager. This helps the operator to deal with less alerts when maintenance is going on, he/she will look into the maintenance alerts only, not all those false alerts fired due to maintenance.

## Setup

* [Installation](installation.md)
* [Configuration](config.md)
* [Testing](testing.md) 

For installation walkthrough of this program go [here](installation.md#intelligence-program).
