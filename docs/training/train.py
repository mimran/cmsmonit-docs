#!/usr/bin/env python

# system modules
import os
import json

# CMSMonitoring modules
from CMSMonitoring.StompAMQ import StompAMQ

def records():
    "example of function which can generate JSON records"
    for i in range(10):
        doc = {"key":i, "data":"data-{}".format(i), "user":os.getenv("USER")}
        doc = {"key":i, "data":"updated-data-{}".format(i), "user":os.getenv("USER")}
        yield doc

def credentials(fname=None):
    "Read credentials from MONIT_BROKER environment"
    if  not fname:
        fname = os.environ.get('MONIT_BROKER', '')
    if  not os.path.isfile(fname):
        raise Exception("Unable to locate MONIT credentials, please setup MONIT_BROKER")
        return {}
    with open(fname, 'r') as istream:
        data = json.load(istream)
    return data

# REPLACE THIS line with list of your JSON records
# you may fetch them from a file, from DB, from other source
# but you should be able to provide this list. In this example
# we will use our custom records function which provides them.
documents = records()

# read our credentials
creds = credentials()
host, port = creds['host_and_ports'].split(':')
port = int(port)
# we will authenticate with our grid certificate
# but we still need to pass username and password to StompAMQ
# therefore we'll use empty strings for them
username = ""
password = ""
# please do not copy certificates anywhere, they are only valid for training
ckey = 'creds/robot-training-key.pem'
cert = 'creds/robot-training-cert.pem'
producer = creds['producer']
topic = creds['topic']
print("producer: {}, topic {}".format(producer, topic))
print("ckey: {}, cert: {}".format(ckey, cert))
# create instance of StompAMQ object with your credentials
amq = StompAMQ(username, password,
               producer, topic,
               key=ckey, cert=cert,
               validation_schema=None, host_and_ports=[(host, port)])

# loop over your document records and create notification documents
# we will send to MONIT
data = []
for doc in documents:
    # every document should be hash id
    hid = doc.get("hash", 1) # replace this line with your hash id generation
    notification, _, _ = amq.make_notification(doc,"training_document", docId=hid)
    data.append(notification)

# send our data to MONIT
results = amq.send(data)
print("results", results)

