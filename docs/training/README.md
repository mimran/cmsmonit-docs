# Tutorials

## CMS Tutorials

* Step-by-step [tutorial](data_injection.md) on 
    - data injection with Python, 
    - lookup with the monit [CLI tool](../infrastructure/README.md#cms-monitoring-cli-tools), 
    - from lxplus for HDFS;
* [Live](https://indico.cern.ch/event/898664/) tutorial (23.3.2020):
    - [data injection](https://gitlab.cern.ch/cmsmonitoring/docs/-/blob/master/docs/training/tutorials/01.DataInjection.ipynb) using a SWAN notebook, 
    - [data visualization](tutorials/02.Visualization.md) with Grafana,
    - [Alerts](tutorials/03.AlertsOnGrafana.md) in Grafana.

### [Example queries](../MONIT/examples.md) for MONIT tool

### SWAN and CMSSpark examples

- [How](../MONIT/HDFS.md) to access data on HDFS
- [CMSSpark framework](https://github.com/vkuznet/CMSSpark/wiki)
- [Spark code](https://github.com/nsmith-/cms-working-set) to read data on HDFS for popularity study
- [CMSSpark SWAN notebook](https://github.com/dmwm/CMSSpark/blob/master/src/notebooks/CMSSparkExample.ipynb) on data popularity
- [Swan notebook](https://cernbox.cern.ch/index.php/s/gBClptQGPs80CpP) to read in job data (/project/awg/cms/job-monitoring/avro-snappy/)

### Rumble

- [Documentation](../Rumble/README.md)

## From CERN IT

* [CERN OpenStack Cloud guide](https://clouddocs.web.cern.ch/clouddocs/)
* [CERN MONIT infrastructure](http://monit-docs.web.cern.ch/monit-docs/overview/index.html)

### SWAN and Spark

 - [HDFS access at CERN](https://hadoop-user-guide.web.cern.ch)
    - [Setup](https://hadoop-user-guide.web.cern.ch/hadoop-user-guide/getstart/client_conf_with_puppet_.html) your own VM to act as Hadoop client
    - [Interacting](https://hadoop-user-guide.web.cern.ch/hadoop-user-guide/getstart/client_edge_machine.html) with Hadoop cluster via edge nodes
 - Documentation about the [SWAN](https://github.com/swan-cern/help) service
